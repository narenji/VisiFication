#ifndef _STATE_MANAGER_H
#define _STATE_MANAGER_H

//#include "Robot.h"
#include "State.h"
#include "Visifier.h"

class StateManager
{
private:
    State* mWorkingState = nullptr;
    State* mFirstOfPhase = nullptr;
    State* CreateSnapshot(const Visifier &visifier);
    vector<Robot> mFirstOfPhaseRobots;
    int mCurrentStateId = 0;
    typedef pair < int, int > Edges;

public:
    StateManager(){}
    State* FindEquivalentState( State* currentState );
    bool SaveState( Visifier &visifier , bool isFirstOfPhase, bool flagMain);
    State* GetWorkingState(){ return mWorkingState; }

    map< int, State*> mStates;
    set<Edges> mTransitions;
//    vector < pair < int, int > > mTransitions;

    State* SetWorkingState(State* workingState);

    State* ClearWorkingState();
    void InsertTransition(int stateId);

    void SetFirstOfPhaseToWorkingState( const Visifier &visifier );
    void RestoreFirstOfPhase(Visifier &visifier);
    int GetStatesSize() {
        return  mStates.size();
         }
};

#endif
