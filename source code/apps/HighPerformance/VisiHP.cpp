#include "VisiHP.h"
#include <fstream>
#include <streambuf>
#include "Utility.h"

VisiHP::VisiHP()
{

}
int VisiHP::GetEqualOfCurrentState() const
{
    return mEqualOfCurrentState;
}

Visifier VisiHP::GetVisifier() const
{
    return mVisifier;
}

VisiHP& VisiHP::GetInstance()
{
    static VisiHP instance;
    return instance;
}

bool VisiHP::Init( string worldDataPath, string runningTime )
{
    bool result = true;
    string worldData = "";
    mRunningTime = atoi (runningTime.c_str());
    if( worldDataPath == "" )
    {

        worldData = "Environment\n0 4\n0 0\n3 2\n4 0\n4 4\n1 2\n";
        worldData += "Robots\n2 1.5 3.9 3.7\n2 2.5 .1 .1\n.5 2.25 1 1\n";
        cout<<"Loading a defaul world ...\n"<<worldData<<endl;
    }
    else
    {
        ifstream worldFile( worldDataPath );
        worldData = string( ( istreambuf_iterator<char>( worldFile ) ),
                            istreambuf_iterator<char>() );
    }

    result = mVisifier.Initialize( worldData );
    if( result )
    {
        mVisifier.ComputeRobotsVisibilityArea();
        mVisifier.ConstructWindowdEnvironment();
//        mVisifier.ComputeInternalNodesVisibilityArea();
        mVisifier.FindAllEventPoints();
        mVisifier.ComputeSubEnv();        


        mVisifier.ComputeDualGraph( true );

        for( int robotIndex = 0; robotIndex < mVisifier.GetRobots().size(); robotIndex++ )
        {

            mVisifier.CalculateRobotSequences( robotIndex );
        }

        mStateManager.SaveState( mVisifier, true, true );
        mTotalStatesTested++;
        mTotalDifferentStates++;
        mRobotIndices.resize( mVisifier.GetRobots().size() );
        mStartTime = time( 0 );
        mLastWrittenFile = time( 0 );
    }
    return result;
}

int VisiHP::GetTestedStatesCount()
{
    return mTotalStatesTested;
}

int VisiHP::GetTotalDifferentStates()
{
    return mTotalDifferentStates;
}

long VisiHP::GetRunDuration()
{
    return time(0) - mStartTime;
}

void VisiHP::Update()
{
    cout<<"Started.\n";
    generate( mRobotIndices.begin(), mRobotIndices.end(), IncrementingSequence() );

    unsigned long int depthCount = 0;    

    while( true )
    {
        bool phaseIncomplete = true;

        for( int robotIndex = 0; robotIndex < mVisifier.GetRobots().size(); robotIndex++ )
        {
            mVisifier.MakeRobotDecision( robotIndex );
        }
//        cout << "**************NEW PHASE*****************" << endl;
        while( phaseIncomplete )
        {
            for( vector<int>::iterator permRobotIndex = mRobotIndices.begin();
                 permRobotIndex != mRobotIndices.end(); permRobotIndex++ )      //iteration over perms
            {
                bool reachedGoal = false;
                mActiveRobotIndex = *permRobotIndex;

//                mVisifier.MakeRobotDecision( mActiveRobotIndex );       //DecisionMaking process


                mVisifier.DetermineRobotWindowRanges( mActiveRobotIndex );     //with respect to the new robot's goal
                mVisifier.ComputeRobotsVisibilityArea();
                mVisifier.ConstructWindowdEnvironment();

                //                mVisifier.PrintEnvironmentHalfedges();


                mVisifier.FindAllEventPoints();
                mVisifier.ComputeSubEnv();

                mVisifier.ComputeDualGraph( false );

                mVisifier.CalculateRobotSequences( mActiveRobotIndex );

                mVisifier.GetRobot( mActiveRobotIndex )->GenerateMedianGoals();
                while( !reachedGoal )           //the last created state must be added to ActiveInitStates queue
                {
                    mTotalStatesTested++;
                    reachedGoal = mVisifier.MoveRobot( mActiveRobotIndex );

                    mVisifier.ComputeRobotsVisibilityArea();
                    mVisifier.ConstructWindowdEnvironment();
                    mVisifier.FindAllEventPoints();
                    mVisifier.ComputeSubEnv();

                    mVisifier.ComputeDualGraph( false );

                    bool isNew = mStateManager.SaveState( mVisifier, false, true );
                    if( isNew )
                    {                      
                        mTotalDifferentStates++;
//                        cout << mTotalDifferentStates << endl;
                        mEqualOfCurrentState = -1;
                    }
                    else
                    {
                        mEqualOfCurrentState = mStateManager.GetWorkingState()->GetID();
                    }
                }
            }
            phaseIncomplete = next_permutation( mRobotIndices.begin(), mRobotIndices.end() );
            if( phaseIncomplete )
            {
                mStateManager.RestoreFirstOfPhase( mVisifier );
            }
            else
            {
                mStateManager.SetFirstOfPhaseToWorkingState( mVisifier );
            }
        }

        if( time(0) - mLastWrittenFile > 10 )
        {
            string durTx = "Duration: " + to_string( GetRunDuration() ) + " (s)";
            float ratio = (float) GetTotalDifferentStates() / (float) GetTestedStatesCount();
            durTx += "| ( " + to_string( GetTotalDifferentStates() ) +
                    " / " + to_string( GetTestedStatesCount() ) + " = " +
                    to_string( ratio ) + " )";
            cout<< durTx<<endl;
//            WriteSummaryToFile();
            mLastWrittenFile = time(0);
        }
        ofstream myfile;
        set<Edges> :: iterator itr;
        int labeledStates = 0;
        if( GetRunDuration() > mRunningTime )      //end of the run reaches
        {
            myfile.open ("VisiFication.aut");
            for( map< int, State*>::iterator it = mStateManager.mStates.begin(); it != mStateManager.mStates.end(); it++ )
            {
                if( (*it).second->GetCoveringStatus() )
                {
                    labeledStates++;
                }
                if( (*it).second->GetConnectivityStatus() )
                {
                    labeledStates++;
                }
            }
            string strFirstLine = "des(0," + to_string(mStateManager.mTransitions.size() + labeledStates) + "," + to_string(mStateManager.mStates.size()) + ")\n";
            myfile << strFirstLine;
            for( itr = mStateManager.mTransitions.begin(); itr != mStateManager.mTransitions.end(); itr++ )    //number of iterations equals number of edges
            {
                string strTransitions = "(" + to_string((*itr).first) + ", action, " + to_string((*itr).second) + ")\n";
//                cout << strTransitions;
                myfile << strTransitions;
            }
            for( map< int, State*>::iterator it = mStateManager.mStates.begin(); it != mStateManager.mStates.end(); it++ )
            {
                if( (*it).second->GetConnectivityStatus() )
                {
                    string strConnectivity = "(" + to_string((*it).second->GetID()) + ", " + "Connectivity" + ", " + to_string((*it).second->GetID()) + ")\n";
                    myfile << strConnectivity;
                }
            }
            for( map< int, State*>::iterator it = mStateManager.mStates.begin(); it != mStateManager.mStates.end(); it++ )
            {
                if( (*it).second->GetCoveringStatus() )
                {
                    string strCovering = "(" + to_string((*it).second->GetID()) + ", " + "Covering" + ", " + to_string((*it).second->GetID()) + ")\n";
                    myfile << strCovering;
                }
            }
            myfile.close();
            cout << "aut file is created" << endl;

            exit(0);
        }
    }
}
