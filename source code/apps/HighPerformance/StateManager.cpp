#include "StateManager.h"

State* StateManager::CreateSnapshot( const Visifier& visifier )
{
//    cout<<"Creating Snapshot...\n";
    State* currentState = new State();
    currentState->mDualGraph = new Graph( visifier.GetDualGraph() );
    for( int robotIndex = 0; robotIndex < visifier.GetRobots().size(); robotIndex++)
    {
        vector<RobotEventPoint> forwardEventPoints = visifier.GetRobots()[robotIndex].mForwardEventPoints;
        for( vector<RobotEventPoint>::iterator evenit = forwardEventPoints.begin();
             evenit != forwardEventPoints.end(); evenit++ )
        {
            currentState->mRobotsForward[robotIndex].push_back( *evenit );
        }

        vector<RobotEventPoint> internalEventPoints = visifier.GetRobots()[robotIndex].mInternalEventPoints;
        for( vector<RobotEventPoint>::iterator evenit = internalEventPoints.begin();
             evenit != internalEventPoints.end(); evenit++ )
        {
            currentState->mInternalRobotsCell[robotIndex].push_back( *evenit );
        }


        vector<RobotEventPoint> backwardEventPoints = visifier.GetRobots()[robotIndex].mBackwardEventPoints;
        for( vector<RobotEventPoint>::iterator evenit = backwardEventPoints.begin();
             evenit != backwardEventPoints.end(); evenit++ )
        {
            currentState->mRobotsBackward[robotIndex].push_back( *evenit );
        }
    }
//    cout<<"Snapshot has been created.\n";
    return currentState;
}

State* StateManager::FindEquivalentState( State* currentState )
{
//    cout<<"Searching for equivalent state ...\n";
    State* equivalent = nullptr;
    for(map< int, State* >::iterator statitor = mStates.begin();
        statitor != mStates.end(); statitor++ )
    {
        if( currentState->mDualGraph->IsEqual( (statitor->second)->mDualGraph )
               /* && //currentState->AreNextEventPointsEqual( statitor->second )
                currentState->AreInternalCellsEqual(statitor->second)*/ )
        {
            //cout<<"Found Equivalent ... "<<endl;
            equivalent = statitor->second;
            break;
        }
    }
//    cout<<"Searching finished.\n";
    return equivalent;
}

bool StateManager::SaveState(  Visifier &visifier, bool isFirstOfPhase, bool flagMain )
{
    bool result = true; //True if state is unique
    State* currentState = CreateSnapshot( visifier );
    State* searchResult = FindEquivalentState( currentState );
    if (flagMain)           //insert the edge of the state space
    {
        if( searchResult != nullptr )       //retreive previous state id
        {
//            cout << YEK"" << endl;
            searchResult->DecreaseStateId();
            if ( searchResult->GetID() != mCurrentStateId )
            {
                StateManager::InsertTransition(searchResult->GetID());
            }
        }
        else if( (searchResult == nullptr) )
        {            
            StateManager::InsertTransition(currentState->GetID());
        }
    }
    else
    {
        currentState->DecreaseStateId();
    }
    if( searchResult == nullptr )
    {
        //Found New State
//        cout<< "Found New State ... "<<endl;
        mCurrentStateId = currentState->GetID();
        if( mWorkingState != nullptr )
        {
            mWorkingState->AddNextState( currentState );            
        }
        if (flagMain)
        {
//            cout << "new state: " << currentState->GetID() << endl;
            currentState->SetConnectivityStatus( visifier.IsConnected() );
            currentState->SetCoveringStatus( visifier.IsCovered() );
        }
        mStates[currentState->GetID()] = currentState;
        mWorkingState = currentState;
    }
    else
    {
//        cout<< "Current state is not new! "<<endl;
        mWorkingState->AddNextState( searchResult );
        //StateManager::InsertTransition(currentState->GetID());
        //currentState->SetConnectivityStatus( visifier.IsConnected() );
        //currentState->SetCoveringStatus( visifier.IsCovered() );
        mWorkingState = searchResult;
        mCurrentStateId = searchResult->GetID();
        result = false;
        delete currentState;
    }
    if( isFirstOfPhase )
    {
        mFirstOfPhase = mWorkingState;
        mFirstOfPhaseRobots = visifier.GetRobots();        
    }
    return result;
}

void StateManager::InsertTransition(int stateId)
{
    mTransitions.insert(make_pair(mCurrentStateId, stateId));
//    mTransitions.push_back(make_pair(mCurrentStateId, stateId));
//    cout << mTransitions.front().first << " " << mTransitions.front().second << endl;
//    mTransitions.pop_back();
}

void StateManager::SetFirstOfPhaseToWorkingState( const Visifier &visifier )
{
    mFirstOfPhase = mWorkingState;
    mFirstOfPhaseRobots = visifier.GetRobots();
}

State* StateManager::SetWorkingState(State* workingState)
{
    mWorkingState = workingState;
}


void StateManager::RestoreFirstOfPhase( Visifier &visifier )
{

    //Engine::GetInstance().mRobots = mWorkingState->mRobots;
    for( int robotIndex = 0; robotIndex < mFirstOfPhaseRobots.size(); robotIndex++ )
    {
        visifier.GetRobot(robotIndex)->MoveTo( mFirstOfPhaseRobots[robotIndex].GetPosition() );
        visifier.GetRobot(robotIndex)->mWindows = mFirstOfPhaseRobots[robotIndex].mWindows;
        visifier.GetRobot(robotIndex)->mForwardEventPoints = mFirstOfPhaseRobots[robotIndex].mForwardEventPoints;
        visifier.GetRobot(robotIndex)->mBackwardEventPoints = mFirstOfPhaseRobots[robotIndex].mBackwardEventPoints;
        visifier.GetRobot(robotIndex)->mInternalEventPoints = mFirstOfPhaseRobots[robotIndex].mInternalEventPoints;

    }
    //Engine::GetInstance().mRemainingRobotsIndex = mWorkingState->mRemainingRobots;
    //cout<<"Restored working state."<<endl;
}

State* StateManager::ClearWorkingState()
{
    mWorkingState = nullptr;

    for( map< int, State*>::iterator it = mStates.begin(); it != mStates.end(); it++ )
    {
        delete (*it).second;
    }

    mStates.clear();
}
