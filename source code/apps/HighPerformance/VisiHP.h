#ifndef _VISIHP_H
#define _VISIHP_H
#include <string>
#include <thread>
#include <mutex>

#include <queue>

#include "StateManager.h"

using namespace std;
/**
 * @brief The VisiHP class uses Visification library to analyse visibility properties of an environment.
 *
 */
class IncrementingSequence
{
public:
    // Constructor, just set counter to 0
    IncrementingSequence() : i_(0) {}
    // Return an incrementing number
    int operator() () { return i_++; }
private:
    int i_;
};

class VisiHP
{
private:

    VisiHP();
    Visifier mVisifier;

    queue <State*> mActiveWorkingState;

    queue <Visifier> mActiveInitStates;

    mutex mStateMutex;
    time_t mStartTime = 0;
    StateManager mStateManager;

    StateManager mActiveStateManager;

    typedef pair < int, int > Edges;

    int mEqualOfCurrentState = -1;
    int mTotalStatesTested = 0;
    int mTotalDifferentStates = 0;
    vector<int> mRobotIndices;
    int mActiveRobotIndex = -1;
    time_t mLastWrittenFile;
    int mRunningTime;
public:

    bool Init( string worldDataPath, string runningTime );
    static VisiHP& GetInstance();
    void Update();
    int GetTestedStatesCount();
    int GetTotalDifferentStates();
    long GetRunDuration();
    Visifier GetVisifier() const;
    int GetEqualOfCurrentState() const;
};


#endif
