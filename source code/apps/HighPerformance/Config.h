#ifndef _CONFIG_H
#define _CONFIG_H

class Config
{
private:
    Config();

public:
    static Config& GetInstance();
};


#endif
