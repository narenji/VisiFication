#include "VisiHP.h"
#include "Config.h"
#include "State.h"

//=========================Main=========================//
int main(int argc, char *argv[])
{
    srand( time(0) );
    string dataPath = "";
    string runningTime = "";
    if(argc > 1)
    {
        dataPath = argv[1] ;
        runningTime = argv[2];
    }
    bool result = VisiHP::GetInstance().Init( dataPath, runningTime );
    if(result)
    {
        VisiHP::GetInstance().Update();
    }
    return 0;
}
