#include "Graphics.h"
#include "VisiX.h"
#include "State.h"
#include <math.h>
#include <FreeImage.h>
#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

Graphics& Graphics::GetInstance()
{
    static Graphics instance;
    return instance;
}

void Graphics::Initialize( int argc, char *argv[] )
{
    RemoveDirectory( Graphics::GetInstance().mSnapShotPath );
    CreateDirectory( Graphics::GetInstance().mSnapShotPath );
    mZoom = .8;
    glutInit(&argc, argv);

    /*Setting up  The Display
        /    -RGB color model + Alpha Channel = GLUT_RGBA
        */
    glutInitDisplayMode(GLUT_RGBA|GLUT_SINGLE);

    //Configure Window Postion
    glutInitWindowPosition(50, 25);

    //Configure Window Size
    glutInitWindowSize( Graphics::GetInstance().mWinWidth, Graphics::GetInstance().mWinHeight );


    //Create Window
    glutCreateWindow("Visification");
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable( GL_BLEND );
    glClearColor( 1,1,1,1 );

    glutKeyboardUpFunc( KeyReleased );
    glutSpecialFunc( SpecialKey );
    glutMouseFunc( MouseClicked );
    glutMotionFunc( MouseMove );
    //    glutReshapeFunc(Reshape);
    glutTimerFunc(20, RenderFrame, 0);
    RenderFrame( 1 );
    //    SaveBitmap( "snapshots/state_0.bmp" );
    // Loop require by OpenGL
    glutMainLoop();
}

void Graphics::DrawCircle(float cx, float cy, float r, int num_segments)
{
    glBegin(GL_LINE_LOOP);
    for(int ii = 0; ii < num_segments; ii++)
    {
        float theta = 2.0f * 3.1415926f * float(ii) / float(num_segments);//get the current angle

        float x = r * cosf(theta);//calculate the x component
        float y = r * sinf(theta);//calculate the y component

        glVertex2f(x + cx, y + cy);//output vertex

    }
    glEnd();
}

void Graphics::DrawRect( float x, float y, float width, float height, bool isStrip )
{
    if( isStrip )
    {
        glBegin(GL_LINE_STRIP);
    }
    else
        glBegin(GL_QUADS);
    glVertex2f( x, y );
    glVertex2f( x + width, y );
    glVertex2f( x + width, y + height);
    glVertex2f( x, y + height );
    if( isStrip )
    {
        glVertex2f( x, y );
    }
    glEnd();
}

void Graphics::WriteText(string text, double posX, double posY)
{
    int len, i;
    len = text.size();

    glColor3f( 0, 0, 0 );

    //    glDisable(GL_LIGHTING);
    glRasterPos2f( posX, posY );

    for (i = 0; i < len; i++)
    {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, text[i]);
    }
}



void Graphics::RenderFrame (int delta)
{
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glPushMatrix(); // save the current matrix
    glScaled( Graphics::GetInstance().mZoom * 2 / VisiX::GetInstance().GetWindowDiameter(),
              Graphics::GetInstance().mZoom * 2 / VisiX::GetInstance().GetWindowDiameter(),
              2 / VisiX::GetInstance().GetWindowDiameter() ); // scale the matrix

    glTranslated( -VisiX::GetInstance().GetWindowCenterX() + Graphics::GetInstance().mOffset.first,
                  -VisiX::GetInstance().GetWindowCenterY() + Graphics::GetInstance().mOffset.second, 0 );
    VisiX::GetInstance().Draw();

    glPopMatrix(); // load the unscaled matrix

    glColor4f( 1, 1, 1, .9 );
    float height = .6;
    Graphics::DrawRect( -1 , 1 - height , 2, height);

    double textStartX = -.98;
    double textStartY = 1;
    WriteText( "Click on Robot to set it active, Move the selected robot using Left(Back) and Right(Forward) keys", textStartX, textStartY-=.05 );
    WriteText( "Use Page Down and Page Up keys to move slower and faster.", textStartX, textStartY-=.05 );
    WriteText( "___________________________________________________", textStartX, textStartY-=.05 );
    string durTx = "Duration: " + to_string( VisiX::GetInstance().GetRunDuration() ) + " (s)" + " IsConncected: " + to_string(VisiX::GetInstance().mVisifier.IsConnected()) + " IsCovered: " + to_string(VisiX::GetInstance().mVisifier.IsCovered());
    WriteText( durTx, textStartX, textStartY-=.05 );
    string stateTx = "State[" + to_string( VisiX::GetInstance().GetTestedStatesCount() - 1 ) + "] is ";
    int equalOfState = VisiX::GetInstance().GetEqualOfCurrentState();
    if( equalOfState == -1 )
    {
        stateTx += "new!";
    }
    else
    {
        stateTx += "equal with state[" + to_string( equalOfState ) + "].";
    }
    float ratio = (float) VisiX::GetInstance().GetTotalDifferentStates() /
            (float) VisiX::GetInstance().GetTestedStatesCount();
    stateTx += " ( " + to_string( VisiX::GetInstance().GetTotalDifferentStates() ) +
            " / " + to_string( VisiX::GetInstance().GetTestedStatesCount() ) + " = " +
            to_string( ratio ) + " )";
    WriteText( stateTx, textStartX, textStartY -= .05 );
    vector<string> forbackseq;
    VisiX::GetInstance().GetRobotSeqencesString( forbackseq );
    for( int seqidx = 0; seqidx < forbackseq.size(); seqidx += 2 )
    {
        string forward = forbackseq[seqidx];
        string backward = forbackseq[seqidx+1];
        WriteText( "Robot["+to_string(seqidx/2) + "] forward " +
                   forward , textStartX, textStartY-=.05 );
        WriteText( "Robot["+to_string(seqidx/2) + "] backward " +
                   backward, textStartX, textStartY-=.05 );
    }

    glFlush();
    if( Graphics::GetInstance().mSaveAfterDrawing )
    {
        SaveBitmap( Graphics::GetInstance().mSnapShotPath +
                    "state_" + to_string( VisiX::GetInstance().GetTestedStatesCount() - 1 ) );
        Graphics::GetInstance().mSaveAfterDrawing = false;
    }
    glutTimerFunc(33, RenderFrame, 0);
}

void Graphics::KeyReleased (unsigned char key, int x, int y)
{
    //    Engine::GetInstance().KeyReleased( key );
    if (key == 27)
    { // If they ‘Esc’ key was pressed

        //mContinueWorkCR.notify_one();
        cout<<"Finished!"<<endl;
        exit(0);
    }
    else if( key == 'z')
    {
        Graphics::GetInstance().mZoom *= 1.1;
    }
    else if( key == 'x')
    {
        if(Graphics::GetInstance().mZoom / 2 > .0002 )
            Graphics::GetInstance().mZoom *= .9;
    }
}


void Graphics::SpecialKey(int key, int x, int y)
{
    if( key == GLUT_KEY_LEFT)
    {
        VisiX::GetInstance().MoveActiveRobotLeft();
    }
    else if( key == GLUT_KEY_RIGHT )
    {
        VisiX::GetInstance().MoveActiveRobotRight();
    }
    if( key == GLUT_KEY_UP )
    {
        VisiX::GetInstance().MoveActiveRobotUp();
    }
    else if( key == GLUT_KEY_DOWN )
    {
        VisiX::GetInstance().MoveActiveRobotDown();
    }

    if( key == GLUT_KEY_PAGE_UP )
    {
        VisiX::GetInstance().Faster();
    }
    else if( key == GLUT_KEY_PAGE_DOWN )
    {
        VisiX::GetInstance().Slower();
    }
}
void Graphics::MouseClicked ( int button, int state, int x, int y)
{
    if( ( button == GLUT_LEFT_BUTTON ) && state == GLUT_UP)
    {
        double ox, oy;
        Project2DtoWorld( x, y, ox, oy );
        VisiX::GetInstance().MouseClicked( ox, oy);
    }
    else if ((button == 3) || (button == 4)) // It's a wheel event
    {
        // Each wheel event reports like a button click, GLUT_DOWN then GLUT_UP
        if (state == GLUT_UP) return; // Disregard redundant GLUT_UP events
        if(button == 3)
        {
            Graphics::GetInstance().mZoom *= 1.1;
        }
        else
        {
            if(Graphics::GetInstance().mZoom / 2 > .0002 )
            {
                    Graphics::GetInstance().mZoom *= .9;
            }
        }
    }
    Graphics::GetInstance().mMousePos.first = x;
    Graphics::GetInstance().mMousePos.second = y;
    Graphics::GetInstance().mDragContent =
            ( button == GLUT_LEFT_BUTTON || button == GLUT_MIDDLE_BUTTON ) && state == GLUT_DOWN;
}

void Graphics::SaveBitmapAfterDrawing()
{
    mSaveAfterDrawing = true;
}
void Graphics::SaveBitmap( string path)
{
    // Make the BYTE array, factor of 3 because it's RBG.
    BYTE* pixels = new BYTE[ 3 * Graphics::GetInstance().mWinWidth* Graphics::GetInstance().mWinHeight ];

    glReadPixels(0, 0, Graphics::GetInstance().mWinWidth, Graphics::GetInstance().mWinHeight, GL_RGB, GL_UNSIGNED_BYTE, pixels);

    // Convert to FreeImage format & save to file
    FIBITMAP* image = FreeImage_ConvertFromRawBits(pixels, Graphics::GetInstance().mWinWidth, Graphics::GetInstance().mWinHeight, 3 * Graphics::GetInstance().mWinWidth, 24, 0x0000FF, 0xFF0000, 0x00FF00, false);
    FreeImage_Save(FIF_BMP, image, path.c_str(), 0);
    // Free resources
    FreeImage_Unload(image);

    delete [] pixels;
}

void Graphics::LoadBitmap( string filePath )
{
    FIBITMAP* bitmap = FreeImage_Load(
                FreeImage_GetFileType( filePath.c_str(), 0),
                filePath.c_str() );
    FIBITMAP *pImage = FreeImage_ConvertTo32Bits( bitmap );
    int nWidth = FreeImage_GetWidth(pImage);
    int nHeight = FreeImage_GetHeight(pImage);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, nWidth, nHeight,
                 0, GL_BGRA, GL_UNSIGNED_BYTE, (void*)FreeImage_GetBits(pImage));

    FreeImage_Unload(pImage);
}

void Graphics::Project2DtoWorld( int x, int y, double& ox, double& oy)
{
    glPushMatrix(); // save the current matrix
    glScaled( Graphics::GetInstance().mZoom * 2 / VisiX::GetInstance().GetWindowDiameter(),
              Graphics::GetInstance().mZoom * 2 / VisiX::GetInstance().GetWindowDiameter(),
              2 / VisiX::GetInstance().GetWindowDiameter() ); // scale the matrix

    glTranslated( -VisiX::GetInstance().GetWindowCenterX() + Graphics::GetInstance().mOffset.first,
                  -VisiX::GetInstance().GetWindowCenterY() + Graphics::GetInstance().mOffset.second, 0 );




    GLint viewport[4];
    GLdouble modelview[16],projection[16];
    GLfloat wx=x,wy,wz;
    GLdouble oz;


    glGetIntegerv(GL_VIEWPORT,viewport);
    y=viewport[3]-y;
    wy=y;
    glGetDoublev(GL_MODELVIEW_MATRIX,modelview);
    glGetDoublev(GL_PROJECTION_MATRIX,projection);
    glReadPixels(x,y,1,1,GL_DEPTH_COMPONENT,GL_FLOAT,&wz);
    gluUnProject(wx,wy,wz,modelview,projection,viewport,&ox,&oy,&oz);
    glPopMatrix(); // load the unscaled matrix
}

void Graphics::RemoveDirectory( string path )
{
    fs::path path_to_remove( path );
    if( fs::exists( path_to_remove ) )
    {
        for ( fs::directory_iterator end_dir_it, it( path_to_remove ); it != end_dir_it; ++it)
        {
            fs::remove_all( it->path() );
        }
    }
}

void Graphics::CreateDirectory( string path )
{
    fs::path path_to_create( path );
    if( ! fs::exists( path_to_create ) )
    {
        fs::create_directory( path_to_create );
    }
}

void Graphics::MouseMove(int x, int y) {

    // this will only be true when the left button is down
    if ( Graphics::GetInstance().mDragContent ) {

        Graphics::GetInstance().mOffset.first += float(x - Graphics::GetInstance().mMousePos.first)/
                ( 100 * Graphics::GetInstance().mZoom);
        Graphics::GetInstance().mOffset.second -= float(y - Graphics::GetInstance().mMousePos.second)/
                ( 100 * Graphics::GetInstance().mZoom);
        Graphics::GetInstance().mMousePos.first = x;
        Graphics::GetInstance().mMousePos.second = y;
    }
}
//void Graphics::Reshape(int width, int height)
//{
//    double mRatio = Graphics::GetInstance().mWinWidth / Graphics::GetInstance().mWinHeight;
//    glViewport(0, 0,
//          (height-(width/mRatio))>0 ? width:(int)(height*mRatio),
//      (width-(height*mRatio))>0? height:(int)(width/mRatio));
//}

//Color rgb2hsv(Color c)
//{
//    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
//    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
//    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

//    float d = q.x - min(q.w, q.y);
//    float e = 1.0e-10;
//    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
//}

//Color hsv2rgb(Color c)
//{
//    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
//    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
//    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
//}
