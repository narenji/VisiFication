#include "XRobot.h"
XRobot::XRobot( Point_2 position, Segment_2 path ):
    Robot( position, path )
{
    srand( mID + 2083 );
    mColor.r = rand() % 235;
    mColor.g = 20 + rand() % 215;
    mColor.b = rand() % 255;
}

XRobot::XRobot( Robot robot ):Robot( robot )
{
    srand( mID + 2083 );
    mColor.r = rand() % 235;
    mColor.g = 20 + rand() % 215;
    mColor.b = rand() % 255;
}

void XRobot::Draw()
{
    glColor3ub( mColor.r, mColor.g, mColor.b );
    //Draw windows
    glLineWidth(2.0); // Set line width to 2.0
    glBegin(GL_LINES);
    glColor3ub( mColor.r, mColor.g, mColor.b );
    for (vector<Window>::iterator eit = mWindows.begin();
         eit != mWindows.end();  ++eit)
    {
        double fx = CGAL::to_double( eit->source().x() );
        double fy = CGAL::to_double( eit->source().y() );
        double sx = CGAL::to_double( eit->target().x() );
        double sy = CGAL::to_double( eit->target().y() );

        glVertex2d( fx, fy );
        glVertex2d( sx, sy );
    }
    glEnd();
    for (vector<Window>::iterator eit = mWindows.begin();
         eit != mWindows.end();  ++eit)
    {
        double fx = CGAL::to_double( eit->source().x() );
        double fy = CGAL::to_double( eit->source().y() );
        double sx = CGAL::to_double( eit->target().x() );
        double sy = CGAL::to_double( eit->target().y() );

        pair<int,int> id = eit->mID;
        string edgeLabel = "<"+to_string( id.first )+","+to_string( id.second )+">";
        double txtPosX = ( fx + sx ) / 2 ;
        double txtPosY = ( fy + sy ) / 2 ;
//        cout<< edgeLabel <<" "<<txtPosX<<", "<<txtPosY<<endl;
        Graphics::WriteText( edgeLabel, txtPosX, txtPosY );
    }
    //draw path
//    double fx = CGAL::to_double( mPath.source().x() );
//    double fy = CGAL::to_double( mPath.source().y() );
//    double sx = CGAL::to_double( mPath.target().x() );
//    double sy = CGAL::to_double( mPath.target().y() );
//    glColor3ub( mColor.r, mColor.g, mColor.b );

//    Graphics::DrawRect( fx - .05, fy - .05, .1, .1 , true );
//    Graphics::DrawRect( sx - .05, sy - .05, .1, .1 );
//    glPushAttrib(GL_ENABLE_BIT);
//    // glPushAttrib is done to return everything to normal after drawing

//    glLineStipple(1, 0xA0A0);
//    glEnable(GL_LINE_STIPPLE);
//    glBegin(GL_LINES);



//    glVertex2d( fx, fy );
//    glVertex2d( sx, sy );

//    glEnd();
//    glPopAttrib();
    //Draw robot itself
    glEnable( GL_POINT_SMOOTH );
    glPointSize( mRobotDotSize );
    glBegin( GL_POINTS );
//    glColor3f( (float)mColor.r/255, (float)mColor.g/255, (float) mColor.b/255  );
    double x = CGAL::to_double( mPosition.x() );
    double y = CGAL::to_double( mPosition.y() );

    glVertex2d( x, y );
    glEnd();
    Graphics::WriteText( "R_" + to_string( mID ), x, y );
    //Draw Event Points
    glColor3ub( mColor.r, mColor.g, mColor.b );

    for( vector<RobotEventPoint>::iterator evenit = mForwardEventPoints.begin();
         evenit != mForwardEventPoints.end(); evenit++ )
    {
        double x = CGAL::to_double( evenit->mPosition.x() );
        double y = CGAL::to_double( evenit->mPosition.y() );
        Graphics::DrawCircle( x, y, .1, 30 );
    }
    for( vector<RobotEventPoint>::iterator evenit = mBackwardEventPoints.begin();
         evenit != mBackwardEventPoints.end(); evenit++ )
    {
        double x = CGAL::to_double( evenit->mPosition.x() );
        double y = CGAL::to_double( evenit->mPosition.y() );
        Graphics::DrawCircle( x, y, .1, 30 );
    }
}

void XRobot::ConvertSequenceToString( vector<RobotEventPoint>& sequence, string& output)
{
    output = "";
    for( vector<RobotEventPoint>::iterator evenit = sequence.begin();
         evenit != sequence.end(); evenit++ )
    {
        string epStr;
        evenit->ToString( epStr );
        output += "{ " + epStr + " }";
    }
}
float XRobot::GetRobotDotSize() const
{
    return mRobotDotSize;
}

void XRobot::SetRobotDotSize(float robotDotSize)
{
    mRobotDotSize = robotDotSize;
}


void XRobot::GetForwardSequenceString( string& output )
{
    ConvertSequenceToString( mForwardEventPoints, output );
}

void XRobot::GetBackwardSequenceString( string& output )
{
    ConvertSequenceToString( mBackwardEventPoints, output );
}
