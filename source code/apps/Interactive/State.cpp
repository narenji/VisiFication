#include "State.h"
#include "Visifier.h"


int State::GetID() const
{
    return mID;
}

State::State()
{
    static int id = 0;
    mID = id++;
    //cout<<"Id "<<mID<<" generated for state.\n ";
}

State::~State()
{
    delete mDualGraph;
    mDualGraph = nullptr;
    //mNextStates.clear();
    mRobotsForward.clear();
    mRobotsBackward.clear();
    mInternalRobotsCell.clear();
}

void State::AddNextState( State* state )
{
    //mNextStates[state->GetID()] = state;
}

bool State::AreSequencesEqual( State* state )
{
//     cout<<"Comparing sequences ...\n";
    bool result = true;
    for( int robotIndex = 0; robotIndex < mRobotsForward.size(); robotIndex++ )
    {

        vector<EventPointBase>& curForward = mRobotsForward[robotIndex];
        vector<EventPointBase>& curBackward= mRobotsBackward[robotIndex];
        vector<EventPointBase>& prevForward= state->mRobotsForward[robotIndex];
        vector<EventPointBase>& prevBackward= state->mRobotsBackward[robotIndex];

   //     if ( robotIndex == 1 )
        {
            cout <<  robotIndex <<"  curForward.size(): " << curForward.size() << " , prevForward.size(): " << prevForward.size() << endl;
        }

        if( curForward.size() != prevForward.size() || curBackward.size() != prevBackward.size() )
        {
            result = false;
            break;
        }
    }
    cout << "----------------------" << endl;
    if( result )
    {
        int robotsSize = mRobotsForward.size();
        for( int robotIndex = 0; robotIndex < robotsSize; robotIndex++ )
        {
            vector<EventPointBase>& curForward = mRobotsForward[robotIndex];
            vector<EventPointBase>& curBackward= mRobotsBackward[robotIndex];
            vector<EventPointBase>& prevForward= state->mRobotsForward[robotIndex];
            vector<EventPointBase>& prevBackward= state->mRobotsBackward[robotIndex];

            //Check Forward sequences
            for( int forwardIndex = 0; forwardIndex < curForward.size(); forwardIndex++ )
            {
                if( curForward[forwardIndex] != prevForward[forwardIndex] )
                {
                    result = false;
                    break;
                }
            }
            if( !result )
            {
                break;
            }
            //Check Backward sequences
            for( int backwardIndex = 0; backwardIndex < curBackward.size(); backwardIndex++ )
            {
                EventPointBase re1 = curBackward[backwardIndex];
                EventPointBase re2 = prevBackward[backwardIndex];
                if( re1 != re2 )
                {
                    result = false;
                    break;
                }
            }
            if( !result )
            {
                break;
            }

        }
    }
//     cout<<"Comparing sequences finished.\n";
    return result;
}

bool State::AreNextEventPointsEqual( State* state )
{
    bool result = true;
    int robotsSize = mRobotsForward.size();
    for( int robotIndex = 0; robotIndex < robotsSize; robotIndex++ )
    {
        vector<EventPointBase>& curForward = mRobotsForward[robotIndex];
        vector<EventPointBase>& curBackward= mRobotsBackward[robotIndex];
        vector<EventPointBase>& prevForward= state->mRobotsForward[robotIndex];
        vector<EventPointBase>& prevBackward= state->mRobotsBackward[robotIndex];

        if ( curBackward.size() > 0 && prevBackward.size() > 0 )    //Only compare the first next potential event points
        {
            if( curBackward[0] != prevBackward[0] )
            {
                result = false;
            }
        }
        if ( prevForward.size() > 0 && curForward.size() > 0 )
        {
            if( curForward[0] != prevForward[0] )
            {
                result = false;
            }
        }
        if ( ( curBackward.size() == 0 && prevBackward.size() != 0 ) ||
             ( curBackward.size() != 0 && prevBackward.size() == 0 ) ||
             ( curForward.size() == 0 && prevForward.size() != 0 ) ||
             ( curForward.size() != 0 && prevForward.size() == 0 ) )
        {
            result = false;
        }
    }
    return result;
}

bool State::AreInternalCellsEqual( State* state )
{
    bool result = false;
    int robotsSize = mInternalRobotsCell.size();
    for( int robotIndex = 0; robotIndex < robotsSize; robotIndex++ )
    {
        vector<EventPointBase>& cur = mInternalRobotsCell[robotIndex];
        vector<EventPointBase>& prev= state->mInternalRobotsCell[robotIndex];

//        cout << cur.size() << " " << prev.size() << endl;

        if ( cur.size() != prev.size() )
        {
            result = false;
            return result;
        }
        for( int index1 = 0; index1 < cur.size(); index1++ )
        {
            result = false;
            for( int index2 = 0; index2 < prev.size(); index2++ )
            {
                EventPointBase re1 = cur[index1];
                EventPointBase re2 = prev[index2];
                if( re1 == re2 )
                {
                    result = true;
                }
            }
            if( !result )
            {
                return result;
            }
        }
    }
//    cout << "******EQUAL*******" << endl;
    return result;
}
