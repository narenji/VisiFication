#include "XVisifier.h"
#include "XRobot.h"
#include "XGraph.h"

int XVisifier::GetSelectedRobotIndex() const
{
    return mSelectedRobotIndex;
}

void XVisifier::SetSelectedRobotIndex(int value)
{
    mSelectedRobotIndex = value;
}
XVisifier::XVisifier():Visifier()
{

}

void XVisifier::Draw()
{
    //Draw Environment

    glLineWidth(6.0); // Set line width to 2.0
    glBegin(GL_LINES);
    glColor3ub( 128, 99, 64 );

    Edge_const_iterator eit;
    for (eit = Visifier::mEnvironment.edges_begin(); eit != Visifier::mEnvironment.edges_end(); ++eit)
    {
        double fx = CGAL::to_double( eit->source()->point().x() );
        double fy = CGAL::to_double( eit->source()->point().y() );
        double sx = CGAL::to_double( eit->target()->point().x() );
        double sy = CGAL::to_double( eit->target()->point().y() );

        glVertex2d( fx, fy );
        glVertex2d( sx, sy );
    }
    glEnd();

    glLineWidth(2.0);
    for (eit = Visifier::mEnvironment.edges_begin(); eit != Visifier::mEnvironment.edges_end(); ++eit)
    {
        double fx = CGAL::to_double( eit->source()->point().x() );
        double fy = CGAL::to_double( eit->source()->point().y() );
        double sx = CGAL::to_double( eit->target()->point().x() );
        double sy = CGAL::to_double( eit->target()->point().y() );

        pair<int,int> id = GetEdgeID( Segment_2(eit->source()->point(), eit->target()->point()) );
        string edgeLabel = "<-1,"+to_string( id.second )+">";
        double txtPosX = ( fx + sx ) / 2 ;
        double txtPosY = ( fy + sy ) / 2 ;
//        cout<< edgeLabel <<" "<<txtPosX<<", "<<txtPosY<<endl;
        Graphics::WriteText( edgeLabel, txtPosX, txtPosY );
    }

    Arrangement_2::Vertex_iterator vit;

    int index = 0;
    for (vit = mEnvironment.vertices_begin();
         vit != mEnvironment.vertices_end(); ++vit)
    {
        Graphics::WriteText( to_string( index ),
                             CGAL::to_double(vit->point().x() ), CGAL::to_double(vit->point().y() ) );
        index++;
    }
    //Draw dual graph
    XGraph dual( mDualGraph );
    dual.Draw();
    //Draw Robots
    for(vector<Robot>::iterator robit = mRobots.begin(); robit != mRobots.end(); robit++)
    {
        XRobot xrobo( *robit );
        if( mSelectedRobotIndex == robit->GetID() )
        {
            xrobo.SetRobotDotSize( 18 );
        }
        xrobo.Draw();
    }
}

bool XVisifier::IsSegmentInside( Point_2 current_position,Point_2 goal )
{

    return Visifier::IsSegmentInside(mEnvironment, Segment_2(current_position, goal)) &&
            Visifier::IsOnTheBoundedSide(current_position, goal);    //dest must be laid in the visible area of the current robot's position
                                                               //dest's face is unbounded
}
