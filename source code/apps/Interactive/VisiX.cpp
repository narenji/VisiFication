#include "VisiX.h"


#include <fstream>
#include <streambuf>
#include "Utility.h"
#include "XRobot.h"

VisiX::VisiX()
{

}
int VisiX::GetEqualOfCurrentState() const
{
    return mEqualOfCurrentState;
}

XVisifier VisiX::GetVisifier() const
{
    return mVisifier;
}

VisiX& VisiX::GetInstance()
{
    static VisiX instance;
    return instance;
}

bool VisiX::Init( string worldDataPath )
{
    bool result = true;
    string worldData = "";
    if( worldDataPath == "" )
    {

        worldData = "Environment\n0 4\n0 0\n3 2\n4 0\n4 4\n1 2\n";
        worldData += "Robots\n2 1.5 3.9 3.7\n2 2.5 .1 .1\n.5 2.25 1 1\n";
        cout<<"Loading a defaul world ...\n"<<worldData<<endl;
    }
    else
    {
        ifstream worldFile( worldDataPath );
        worldData = string( ( istreambuf_iterator<char>( worldFile ) ),
                            istreambuf_iterator<char>() );
    }

    result = mVisifier.Initialize( worldData );
    mWinDiameter = mVisifier.mBoundingBoxDiameter;
    mWinCenterX = mVisifier.mBoundingBoxCenter.first;
    mWinCenterY = mVisifier.mBoundingBoxCenter.second;

    mVisifier.ComputeRobotsVisibilityArea();
    mVisifier.ConstructWindowdEnvironment();
//    mVisifier.ComputeInternalNodesVisibilityArea();

    mVisifier.FindAllEventPoints();
    mVisifier.ComputeSubEnv();

//    mVisifier.PrintEnvironmentHalfedges();


    mVisifier.ComputeDualGraph( true );

//    mVisifier.ComputeRobotsSubCells();
    for( int robotIndex = 0; robotIndex < mVisifier.GetRobots().size(); robotIndex++ )
    {

        mVisifier.CalculateRobotSequences( robotIndex );
    }

    mStateManager.SaveState( mVisifier );
    mTotalStatesTested++;
    mTotalDifferentStates++;
    Graphics::GetInstance().SaveBitmapAfterDrawing();
    mStartTime = time( 0 );
    return result;
}

int VisiX::GetTestedStatesCount()
{
    return mTotalStatesTested;
}

int VisiX::GetTotalDifferentStates()
{
    return mTotalDifferentStates;
}

void VisiX::Draw()
{
    mStateMutex.lock();
    mVisifier.Draw();
    mStateMutex.unlock();
}

//void VisiX::StartWorking()
//{
//    mStartTime = time( 0 );
//    mUpdateThread = new thread( &VisiX::Update, this );
//}

long VisiX::GetRunDuration()
{
    return time(0) - mStartTime;
}

void VisiX::Update(int robotIndex)
{
    bool reachedGoal = false;
    mTotalStatesTested++;
    mVisifier.ComputeDualGraph( true );
    mVisifier.FindAllEventPoints();
    mVisifier.ComputeSubEnv();

//    mVisifier.PrintEnvironmentHalfedges();


    for( int robotIndex = 0; robotIndex < mVisifier.GetRobots().size(); robotIndex++ )
    {

        mVisifier.CalculateRobotSequences( robotIndex );
    }

    mVisifier.GetRobot(robotIndex)->GenerateMedianGoals();
    while( !reachedGoal )           //the last created state must be added to ActiveInitStates queue
    {
        mTotalStatesTested++;
        reachedGoal = mVisifier.MoveRobot( robotIndex );

        mVisifier.ComputeRobotsVisibilityArea();
        mVisifier.ConstructWindowdEnvironment();
//        mVisifier.ComputeInternalNodesVisibilityArea();

        mVisifier.FindAllEventPoints();
        mVisifier.ComputeSubEnv();

        mVisifier.ComputeDualGraph( true );

        for( int robotIndex = 0; robotIndex < mVisifier.GetRobots().size(); robotIndex++ )
        {
            mVisifier.CalculateRobotSequences( robotIndex );
        }
        bool isNew = mStateManager.SaveState( mVisifier );
        if( isNew )
        {
            mTotalDifferentStates++;
            mEqualOfCurrentState = -1;
            Graphics::GetInstance().SaveBitmapAfterDrawing();
        }
        else
        {
            mEqualOfCurrentState = mStateManager.GetWorkingState()->GetID();
        }
    }
}

void VisiX::GetRobotSeqencesString( vector<string>& sequences )
{
    vector<Robot> robots = mVisifier.GetRobots();
    sequences.clear();
    for( int robotIndex = 0; robotIndex < robots.size(); robotIndex++ )
    {
        XRobot robot( robots[robotIndex] );
        string back, forw;
        robot.GetForwardSequenceString( forw );
        robot.GetBackwardSequenceString( back );

        sequences.push_back( forw );
        sequences.push_back( back );
    }
}

void VisiX::MouseClicked( double x, double y )
{
    cout<<"Should check "<<x<<" "<<y<<endl;
    Point_2 mousePosition( x, y );
    double minDistance = INT_MAX;
    int minDistanceRobotIdx = -1;
    for( int robotIndex = 0; robotIndex < mVisifier.GetRobots().size();
         robotIndex++ )
    {
        Vector_2 distVec = mousePosition - mVisifier.GetRobot( robotIndex )->GetPosition();
        double dist = CGAL::to_double( distVec.squared_length() );
        if( dist < minDistance )
        {
            minDistanceRobotIdx = robotIndex;
            minDistance = dist;
        }

    }
    if( minDistance < 0.01 )
    {
        mVisifier.SetSelectedRobotIndex( minDistanceRobotIdx );
    }
    else
    {
        mVisifier.SetSelectedRobotIndex( -1 );
    }
}

//void VisiX::MoveActiveRobotForward()
//{

//    if( mVisifier.GetSelectedRobotIndex() != -1 )
//    {

//        double progress = mVisifier.GetRobot(mVisifier.GetSelectedRobotIndex())->GetProgress() + 1;
//        if( progress <= 100 )
//        {
//            mVisifier.GetRobot(mVisifier.GetSelectedRobotIndex())->MoveTo( progress );
//            Update();
//        }
//    }

//}

//void VisiX::MoveActiveRobotBackward()
//{
//    if( mVisifier.GetSelectedRobotIndex() != -1 )
//    {
//        double progress = mVisifier.GetRobot(mVisifier.GetSelectedRobotIndex())->GetProgress() - 1;
//        if( progress >= 0 )
//        {
//            mVisifier.GetRobot(mVisifier.GetSelectedRobotIndex())->MoveTo( progress );
//            Update();
//        }
//    }
//}

void VisiX::MoveActiveRobotLeft()
{
    if( mVisifier.GetSelectedRobotIndex() != -1 )
    {
        Point_2 pos = mVisifier.GetRobot(mVisifier.GetSelectedRobotIndex())->GetPosition();
        Point_2 goal( pos.x() - mMoveSpeed , pos.y() );


        if( mVisifier.IsSegmentInside( pos, goal ) )
        {
            mVisifier.GetRobot(mVisifier.GetSelectedRobotIndex())->SetGoal( goal );
            mVisifier.DetermineRobotWindowRanges( mVisifier.GetSelectedRobotIndex() );
            //mVisifier.GetRobot(mVisifier.GetSelectedRobotIndex())->MoveTo( goal );
            Update(mVisifier.GetSelectedRobotIndex());
        }
    }
}

void VisiX::MoveActiveRobotRight()
{
    if( mVisifier.GetSelectedRobotIndex() != -1 )
    {
        Point_2 pos = mVisifier.GetRobot(mVisifier.GetSelectedRobotIndex())->GetPosition();
        Point_2 goal( pos.x() + mMoveSpeed , pos.y() );


        if( mVisifier.IsSegmentInside( pos, goal ) )
        {
            mVisifier.GetRobot(mVisifier.GetSelectedRobotIndex())->SetGoal( goal );
            mVisifier.DetermineRobotWindowRanges( mVisifier.GetSelectedRobotIndex() );
            //mVisifier.GetRobot(mVisifier.GetSelectedRobotIndex())->MoveTo( goal );
            Update(mVisifier.GetSelectedRobotIndex());
        }
    }
}

void VisiX::MoveActiveRobotUp()
{
    if( mVisifier.GetSelectedRobotIndex() != -1 )
    {
        Point_2 pos = mVisifier.GetRobot(mVisifier.GetSelectedRobotIndex())->GetPosition();
        Point_2 goal( pos.x()  , pos.y() + mMoveSpeed );


        if( mVisifier.IsSegmentInside( pos, goal ) )
        {
            mVisifier.GetRobot(mVisifier.GetSelectedRobotIndex())->SetGoal( goal );
            mVisifier.DetermineRobotWindowRanges( mVisifier.GetSelectedRobotIndex() );
            //mVisifier.GetRobot(mVisifier.GetSelectedRobotIndex())->MoveTo( goal );
            Update(mVisifier.GetSelectedRobotIndex());
        }
    }
}
void VisiX::MoveActiveRobotDown()
{
    if( mVisifier.GetSelectedRobotIndex() != -1 )
    {
        Point_2 pos = mVisifier.GetRobot(mVisifier.GetSelectedRobotIndex())->GetPosition();
        Point_2 goal( pos.x()  , pos.y() - mMoveSpeed );


        if( mVisifier.IsSegmentInside( pos, goal ) )
        {
            mVisifier.GetRobot(mVisifier.GetSelectedRobotIndex())->SetGoal( goal );
            mVisifier.DetermineRobotWindowRanges( mVisifier.GetSelectedRobotIndex() );
            //mVisifier.GetRobot(mVisifier.GetSelectedRobotIndex())->MoveTo( goal );
            Update(mVisifier.GetSelectedRobotIndex());
        }
    }
}

void VisiX::Faster()
{

    mMoveSpeed *= 2;
}

void VisiX::Slower()
{
    if(mMoveSpeed/2 >= 0.001)
        mMoveSpeed /= 2;
}
