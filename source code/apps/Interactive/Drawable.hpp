#ifndef _DRAWABLE_H
#define _DRAWABLE_H

#include "Graphics.h"

class Drawable
{
private:

public:
    virtual void Draw() = 0;
};


#endif
