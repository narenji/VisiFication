#ifndef _VISIX_H
#define _VISIX_H
#include <string>
#include "XVisifier.h"
#include "Drawable.hpp"
#include <thread>
#include <mutex>
#include "StateManager.h"

using namespace std;
/**
 * @brief The VisiX class uses Visification library to graphically show analysis of visibility properties of an environment.
 *
 */

class VisiX : Drawable
{
private:

    VisiX();
    //XVisifier mVisifier;
    float mWinDiameter = 100;
    float mWinCenterX = 0;
    float mWinCenterY = 0;
    mutex mStateMutex;
    time_t mStartTime = 0;
    StateManager mStateManager;
    int mEqualOfCurrentState = -1;
    int mTotalStatesTested = 0;
    int mTotalDifferentStates = 0;
    float mMoveSpeed =.001;
public:

    XVisifier mVisifier;

    bool Init( string worldDataPath );
    static VisiX& GetInstance();
    void Update( int robotIndex );
    void Draw();
    float GetWindowDiameter(){ return mWinDiameter;}
    float GetWindowCenterX(){ return mWinCenterX;}
    float GetWindowCenterY(){ return mWinCenterY;}
    int GetTestedStatesCount();
    int GetTotalDifferentStates();
//    void StartWorking();
    long GetRunDuration();
    void GetRobotSeqencesString( vector<string>& sequences );
    void MouseClicked( double x, double y );
//    void MoveActiveRobotForward();
//    void MoveActiveRobotBackward();
    void MoveActiveRobotLeft();
    void MoveActiveRobotRight();
    void MoveActiveRobotUp();
    void MoveActiveRobotDown();
    XVisifier GetVisifier() const;
    int GetEqualOfCurrentState() const;
    void Faster();
    void Slower();
};


#endif
