#ifndef _X_VISIFIER_H
#define _X_VISIFIER_H
#include "Visifier.h"
#include "Drawable.hpp"

class XVisifier : public Visifier, public Drawable
{
private:

    int mSelectedRobotIndex = -1;
public:
    XVisifier();
    virtual void Draw();
    void StartWorking();
    int GetSelectedRobotIndex() const;
    void SetSelectedRobotIndex(int value);
    bool IsSegmentInside(Point_2 current_position, Point_2 goal);
};


#endif
