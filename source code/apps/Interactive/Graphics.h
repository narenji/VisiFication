#ifndef _GRAPHICS_H
#define _GRAPHICS_H
//#ifdef GLENABLED
#include <GL/glut.h>
//#endif
#include <iostream>
#include <string>

using namespace std;

struct Color
{
    GLint r,g,b;
};

class Graphics
{
private:
    float mZoom;
    int mWinWidth = 1024;
    int mWinHeight = 768;
    bool mSaveAfterDrawing = false;
    bool mDragContent = false;
    pair<float, float> mMousePos, mOffset;
    string mSnapShotPath = "snapshots/";
    Graphics(){}
public:
    static Graphics& GetInstance();
    void Initialize(int argc, char *argv[]);
    static void WriteText(string text, double posX, double posY);
    static void RenderFrame (int delta);
    static void KeyReleased (unsigned char key, int x, int y);
    static void SpecialKey(int key, int x, int y);
    static void MouseClicked (int button, int state, int x, int y);
    static void DrawCircle( float cx, float cy, float r, int num_segments);
    static void DrawRect(float x, float y, float width, float height , bool isStrip = false);
    static void SaveBitmap( string path);
    static void LoadBitmap( string filePath );
    void SaveBitmapAfterDrawing();
    static void Project2DtoWorld( int x, int y, double& ox, double& oy);
    static void RemoveDirectory( string path );
    static void CreateDirectory( string path );

    static void MouseMove(int x, int y);
//    static void Reshape(int width, int height);
};

#endif
