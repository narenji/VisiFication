#ifndef _X_GRAPH_H
#define _X_GRAPH_H
#include "Graph.h"
#include "Drawable.hpp"

class XGraph: public Graph, public Drawable
{
private:
    Color mColor;
    vector< pair<double, double> > mRepresentivePoints;
public:
    using Graph::Graph;
    virtual void Draw();
};


#endif
