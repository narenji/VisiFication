#ifndef _STATE_MANAGER_H
#define _STATE_MANAGER_H

//#include "Robot.h"
#include "State.h"
#include "Visifier.h"

class StateManager
{
private:
    map< int, State*> mStates;
    State* mWorkingState = nullptr;
    State* mFirstOfPhase = nullptr;
    State* CreateSnapshot(const Visifier &visifier);
    vector<Robot> mFirstOfPhaseRobots;
public:
    StateManager(){}
    State* FindEquivalentState( State* currentState );
    bool SaveState( const Visifier &visifier );
    State* GetWorkingState(){ return mWorkingState; }
    void SetFirstOfPhaseToWorkingState( const Visifier &visifier );
    void RestoreFirstOfPhase(Visifier &visifier);
    int GetStatesSize() {
        return  mStates.size();
         }
};

#endif
