#ifndef _X_ROBOT_H
#define _X_ROBOT_H
#include "Robot.h"
#include "Drawable.hpp"

class XRobot: public Robot, public Drawable
{
private:
    Color mColor;
    void ConvertSequenceToString( vector<RobotEventPoint>& sequence, string& output);
    float mRobotDotSize = 4;
public:
    XRobot(Point_2 position, Segment_2 path);
    XRobot(Robot robot);
    virtual void Draw();
    void GetForwardSequenceString( string& output );
    void GetBackwardSequenceString( string& output );
    float GetRobotDotSize() const;
    void SetRobotDotSize(float robotDotSize);
};


#endif
