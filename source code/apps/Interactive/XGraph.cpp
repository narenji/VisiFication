#include "XGraph.h"

void XGraph::Draw()
{
    map< int, pair<double, double> > nodeIndexToPoint;
    for( int nodeIndex = 0; nodeIndex < mSize; nodeIndex++ )
    {
        //Draw nodes
        Point_2 pos = mNodes[nodeIndex].mRepresentative;
        glEnable( GL_POINT_SMOOTH );
        glPointSize( 2 );
        glBegin( GL_POINTS );

        double x = CGAL::to_double( pos.x() );
        double y = CGAL::to_double( pos.y() );
        nodeIndexToPoint[nodeIndex] = pair<double, double>( x, y );
        glVertex2d( x, y );
        glEnd();
        Graphics::WriteText( "G_" + to_string( nodeIndex ),x , y );
    }
    for( int nodeIndex = 0; nodeIndex < mSize; nodeIndex++ )
    {
        //Draw nodes
        vector< int > neighbors = mNodes[nodeIndex].mNeighbors;
        glLineWidth(2.0); // Set line width to 2.0
        glBegin(GL_LINES);
        for (vector<int>::iterator eit = neighbors.begin();
             eit != neighbors.end();  ++eit)
        {
            double fx = nodeIndexToPoint[nodeIndex].first;
            double fy = nodeIndexToPoint[nodeIndex].second;
            double sx = nodeIndexToPoint[*eit].first;
            double sy = nodeIndexToPoint[*eit].second;

            glVertex2d( fx, fy );
            glVertex2d( sx, sy );
        }
        glEnd();
    }
}
