#ifndef _STATE_H
#define _STATE_H
#include "EventPointBase.h"
#include "Graph.h"

class State
{
private:
    map< int, State* > mNextStates;
    int mID = -1;
public:
    State();
    ~State();
    Graph* mDualGraph = nullptr;
    map< int, vector<EventPointBase> >  mRobotsForward, mRobotsBackward;
    map< int, vector<EventPointBase> >  mInternalRobotsCell;
    void AddNextState(State* state);
    bool AreInternalCellsEqual( State* state );
    bool AreSequencesEqual( State* state );
    bool AreNextEventPointsEqual( State* state );

    int GetID() const;
};

#endif
