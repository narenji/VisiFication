# VisiFication Project

We implemented a library called __VisiFication__ using Computational Geometry Algorithms Library (CGAL)
in order to automatically
generate a discrete state space of a given robot system. This
allows using existing model checking tools (e.g., CADP) to guarantee the correctness of the desired requirements. We
construct the state space of a number of robots, each moves
freely within a bounded polygonal
area. This state space can then be used to verify visibility
properties (e.g., if the robots can collectively see the entire area).

## Code Structure

VisiFication Project contains core functionalities of the program and is located in a directory named __lib__.
To show the usage of VisiFication library, we developed two applications (__HighPerformance__, which is used to achieve the experimental results mentioned in the paper, and __Interactive__, a graphical interface is provided in which a user make the robots' decisions and the new states will be generated respectively) which can be found in __app__ directory.

### Sample Code (HighPerformance application)
We prepared a simple example to show the usage of VisiFication Library. 
The first step is to instantiate Visifier and StateManager classes.
Then we initialize Visifier with world data string.
The World data string composed of two parts. The first part starts with a line containing **Environment**
followed by line segments containing vertices of the polygonal boundary as well as the boundaries of the obstacles. The second part starts with **Robots** followed by the positions where the robots reside in.
Then, we check whether the initialization is executed properly, and then compute the dual graph of the subdivision of the environment.
At the next step, all the possible event points which make the transitions to next states are computed (transition types (a), (b) and (c), as mentioned in the paper).
The final step is to calculate the dual graph of the subdivision obtained by windows of the robots' windows intersection points.
We save the computed current state as the initial state in the __State Graph__.

Based on the created initial state, the robots start to make decision and move toward their destination points. 
During the movement, some event points may be reached. So, the corresponding states are then created. 
The variable __isNew__ indicates whether the computed state is new.  


``` c++
#include "Visifier.h"
#include "StateManager.h"

int main(int argc, char *argv[])
{
	// 0. Instansiate Visifier and StateManager
    Visifier mVisifier;
    StateManager mStateManager;

	//1. Initialize visifier
    string worldData = "Environment\n0 4\n0 0\n3 2\n4 0\n4 4\n1 2\n"
                       "Robots\n2 1.5 3.9 3.7\n2 2.5 .1 .1\n.5 2.25 1 1\n";

    bool result = mVisifier.Initialize( worldData, runningTime );
    if( result )
    {
		// 2. build and save root of state graph
	mVisifier.ComputeRobotsVisibilityArea();
        mVisifier.ConstructWindowdEnvironment();
        mVisifier.FindAllEventPoints();
        mVisifier.ComputeSubEnv(); 
        mVisifier.ComputeDualGraph( false );
        mVisifier.FindAllEventPoints();
        for( int robotIndex = 0; robotIndex < mVisifier.GetRobots().size(); robotIndex++ )
        {
            mVisifier.CalculateRobotSequences( robotIndex );
        }
		
        mStateManager.SaveState( mVisifier, true );
    }
    
    //3. Make robot decision and compute and save median states
    for( int mActiveRobotIndex = 0;
         mActiveRobotIndex < mVisifier.GetRobots().size(); mActiveRobotIndex++ )
    {
        mVisifier.MakeRobotDecision( mActiveRobotIndex );
        bool reachedGoal = false;
        mVisifier.GetRobot( mActiveRobotIndex )->GenerateMedianGoals();
        while( !reachedGoal )
        {
            reachedGoal = mVisifier.MoveRobot( mActiveRobotIndex );
	    mVisifier.ComputeRobotsVisibilityArea();
            mVisifier.ConstructWindowdEnvironment();
            mVisifier.FindAllEventPoints();
            mVisifier.ComputeSubEnv(); 
            mVisifier.ComputeDualGraph( false );
            mVisifier.FindAllEventPoints();
            for( int robotIndex = 0; robotIndex < mVisifier.GetRobots().size(); robotIndex++ )
            {
                mVisifier.CalculateRobotSequences( robotIndex );
            }

            bool isNew = mStateManager.SaveState( mVisifier, false );
            if( isNew )
                cout<<"state is new!"<<endl;
            else
                cout<<"state is not new!"<<endl;
        }
    }

    return 0;
}

```


To see a complete example check code for ***HighPerformance*** application.
## Build Requirements

* Debian 8, CMake 2.8+
* CGAL-4.7+
* GLUT Library (essential for Interactive application)

