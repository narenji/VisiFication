#ifndef _EVENTPOINT_H
#define _EVENTPOINT_H
#include "EventPointBase.h"
#include "CGALHelper.hpp"

using namespace std;

class EventPoint: public EventPointBase
{
public:
    Point_2 mPosition;

};

#endif
