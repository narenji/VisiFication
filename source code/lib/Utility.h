#ifndef _UTILITY_H
#define _UTILITY_H
#include "CGALHelper.hpp"

namespace Utility
{

class IncrementingSequence
{
public:
    // Constructor, just set counter to 0
    IncrementingSequence() : i_(0) {}
    // Return an incrementing number
    int operator() () { return i_++; }
private:
    int i_;
};


template <typename T> int sgn(T val)
{
    return (T(0) < val) - (val < T(0));
}


double AngleBetweenThreePoints( Point_2 p1, Point_2 middlePoint, Point_2 p3 );
bool IsPointInsideSemiCircle( double radius, double centerX,double centerY,
                              double startAngle , double endAngle ,
                              double pointx  , double  pointy  );
double Cross( Vector_2 vec1, Vector_2 vec2);
void MakeTriangleCCW(Point_2 &p1, const Point_2& middlePoint, Point_2 &secondEndPoint );


enum IntersectionType
{
    IT_NONE,
    IT_POINT,
    IT_SEGMENT
};
enum PointPlacementType
{
    PPT_BEFORE_FIRST,
    PPT_BETWEEN,
    PPT_AFTER_SECOND,
    PPT_OUT
};

IntersectionType FindIntersection(Segment_2 first, Segment_2 second, CGAL::Object& intersection );
Point_2 ExtendSegmentFromEnd( Point_2 segmentStart, Point_2 segmentEnd, double toSize, bool inOppositeDirection);
PointPlacementType CheckPointPlacement( Point_2 checkingPoint, Point_2 firstPoint, Point_2 secondPoint );
bool CheckInside(Point_2 checkingPoint, Point_2 firstEndPoint, Point_2 middlePoint, Point_2 secondEndPoint);
}//End of namespace

#endif
