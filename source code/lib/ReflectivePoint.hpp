#ifndef _REFLECTIVE_POINT_H
#define _REFLECTIVE_POINT_H
#include "CGALHelper.hpp"

struct ReflectivePoint
{
    Point_2 mPosition;
    int mIndexInEnvironment;
    // First and Second edge in CCW order:
    Point_2 mFirstEdgeStartCCW;
    Point_2 mSecondEdgeEndCCW;

    Point_2 mFirstEdgeExtendedStartCCW;
    Point_2 mSecondEdgeExtendedEndCCW;

    Point_2 mFirstEdgeOpposite;
    Point_2 mSecondEdgeOpposite;

};

#endif
