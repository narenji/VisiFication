#ifndef _GRAPH_H
#define _GRAPH_H
#include <vector>
#include "CGALHelper.hpp"
using namespace std;

struct GraphNode
{
    ~GraphNode();
    vector< int > mNeighbors;
    vector< pair<pair< pair< int, int >, pair< int, int > >, int > > mCellInfo;
    Point_2 mRepresentative;
    void Draw();
};

class Graph
{
protected:
    GraphNode* mNodes;
    int mSize = 0;
public:
    Graph( int size );
    Graph( Graph* graph );
    ~Graph();
    void Draw();
    void AddEdge( int from, int to );
    //bool operator==( const Graph& rhs );
    bool IsEqual( Graph* graph);
    int GetSize() const;
    GraphNode* GetNodes() const;
};

#endif
