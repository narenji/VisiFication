#ifndef _VISIFIER_H
#define _VISIFIER_H
#include <string>
#include <vector>
#include <cmath>
//#include <thread>
//#include <mutex>
//#include <condition_variable>
#include "Robot.h"
#include "CGALHelper.hpp"
#include "Graph.h"
#include "ReflectivePoint.hpp"
#include "CGAL/intersections.h"
#include <CGAL/Nef_polyhedron_2.h>
#include <CGAL/Exact_rational.h>
#include <CGAL/Lazy_exact_nt.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/Bounded_kernel.h>
#include <CGAL/Nef_polyhedron_2.h>
#include <CGAL/Arr_segment_traits_2.h>
#include <CGAL/Arrangement_2.h>
#include <CGAL/Arr_walk_along_line_point_location.h>
//#include <CGAL/Arr_point_location_result.h>

using namespace std;
/**
 * @brief The Visifier class is where analysis of visibility properties happens.
 */
class Visifier
{

private:
    typedef CGAL::Exact_predicates_exact_constructions_kernel K;
    typedef Arrangement_2::Halfedge_const_handle Halfedge_const_handle;
    typedef K::Point_2 Point;
    pair< pair< pair< int, int >, pair< int, int > >, int > internalEdgeID;

protected:



    bool IsWindow(Segment_2 edge);




    void SetRobotWindow( int robotIndex, Arrangement_2& visibilityArea );
    void SetInterNodeWindow( Point_2 interNodeLocation, pair<int,int> srcEdgeID, pair<int,int> destEdgeID, Arrangement_2 visibilityArea );



//    void Update();
//    thread* mUpdateThread;
//    bool mFinished = false;

    Arrangement_2 mEnvironment;
    Polygon_2 mEnvironmentPolygon;
    Polygon_with_holes_2 mEnvironmentHolePolygon;
    Arrangement_2 mSubEnv;
    Arrangement_2 mWindowdEnvironment;
    vector<Robot> mRobots;
    map< pair<Kernel::FT, Kernel::FT>, int> mPointToIndexMap;
    map< pair <pair<Kernel::FT, Kernel::FT>, pair<Kernel::FT, Kernel::FT> >, int > mEdgeToIndexMap;
    int EdgeToIndex( Segment_2 edge );
    int VertexToIndex( Point_2 vertex);
    Graph* mDualGraph = nullptr;
    vector<EventPoint> mEventPoints, mWindowEventpoints;

    map< pair <pair<Kernel::FT, Kernel::FT>, pair<Kernel::FT, Kernel::FT> >, pair<int,int> > mWindowIDMap;
    map< pair <pair<Kernel::FT, Kernel::FT>, pair<Kernel::FT, Kernel::FT> >, pair< pair< pair< int, int >, pair< int, int > >, int > > mInternalWindowIDMap;

    bool AreConnectable(Point_2 point1, Point_2 point2);
    bool visited[ ];

    bool IsSelfWindow(vector<EventPoint>::iterator eventitor, Point_2 reflectivePoint );

//    int mTotalStates = 0;
//    int mActiveRobotIndex = -1;


    vector<RobotEventPoint> GetFutureEventPoints(ReflectivePoint reflectivePoint, Segment_2 path );
//    void TestFunction();
//    mutex mStateMutex;
//    mutex mContinueWorkMutex;
//    condition_variable mContinueWorkCR;
//    bool mFinishedStates = false;

    bool IsReflective(Point_2 firstPoint, Point_2 middlePoint, Point_2 lastPoint);
    map< int, ReflectivePoint > mReflectivePoints;
    void FindAllReflectives();
//    vector<int> mRobotIndices;


    void SetMoveRobotForward(int robotIndex);
    void SetMoveRobotBackward(int robotIndex);
    int GetNumberOfVisibleRobots(int robotIndex);


public:
    Visifier();
    double Determinan(Point_2 p);
    void DetermineRobotWindowRanges( int robotIndex );
    bool IsOnTheBoundary(Point_2 q);
    bool IsOnTheBoundedSide(Point_2 s, Point_2 t);
    void ComputeRobotsVisibilityArea();
    void ComputeInternalNodesVisibilityArea();
    Arrangement_2 GetInterNodeVisibilityArea( Point_2 interNodeLocation );
    void ComputeRobotsSubCells();
    void ComputeSubEnv();
    void ConstructWindowdEnvironment();
    pair< int, int > GetEdgeID( Segment_2 edge);
    pair< pair< pair< int, int >, pair< int, int > >, int > GetInternalEdgeID( Segment_2 edge);

    double mBoundingBoxDiameter;
    pair< double, double> mBoundingBoxCenter;
    bool Initialize( string worldData );
    bool LoadData(string worldData);
    void MakeRobotDecision( int robotIndex );
    void RefreshMedianGoals( int activeRobotIndex );
    vector<Robot> GetRobots() const;
    Robot* GetRobot( int robotIndex );
    void FindAllEventPoints();
    void CalculateRobotSequences(int robotIndex);
    void ComputeDualGraph(bool computeNodeRepresentative = false);
    bool MoveRobot(int activeRobotIndex);
//    bool AreVisible(int robotIndex1, int robotIndex2);
    void DFSUtil(int v, vector<vector<int>> adj);
    bool IsConnected();
    bool IsCovered();    
    Arrangement_2 ComputeVisibilityPolygon( Arrangement_2 env, Point_2 query );
    bool IsSegmentInside( Arrangement_2 env, Segment_2 seg );
    bool IsSegmentInsideIrregular( Arrangement_2 env, Segment_2 seg );
    void PrintFaceHalfEdges(Arrangement_2 env, int robotIndex);
    void PrintEnvironmentHalfedges();





    Graph * GetDualGraph() const;
    void SetDualGraph(Graph *dualGraph);

    unsigned long int depth = 0;
};

#endif
