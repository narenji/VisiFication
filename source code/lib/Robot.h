﻿#ifndef _ROBOT_H
#define _ROBOT_H
#include "CGALHelper.hpp"
#include "RobotEventPoint.hpp"
#include "ReflectivePoint.hpp"
#include "ReflectiveToPathMapPoint.hpp"
#include "Window.hpp"
using namespace std;

class Robot
{
private:
    vector< Point_2 > mMedianGoals;
    void RemoveSelfieEventpoints( vector<RobotEventPoint>& eventpoints );
protected:
    int mID;
    double mProgress = 0;

public:
    Point_2 mPosition;
    Arrangement_2 mVisibilityArea;
    Point_2 GetPossibleDest( Segment_2 potentialDest );
//    COLOR mColor;
    Segment_2 mPath;
    vector<RobotEventPoint> mInternalEventPoints;
    vector<RobotEventPoint> mForwardEventPoints;
    vector<RobotEventPoint> mBackwardEventPoints;
    void SortSequence( vector<RobotEventPoint>& sequence );
    bool mReachedGoal = true;
    Point_2 mGoal;
    vector<Window> mWindows;
    Robot(Point_2 position, Segment_2 path);
//    Robot(const Robot& robot);
    Point_2 GetPosition() const;
    void SetPosition(const Point_2 &position);
    Arrangement_2 GetVisibilityArea() const;
    void SetVisibilityArea(const Arrangement_2 &visibilityArea);
    vector<Window> &GetWindows();
//    void Draw();
    Segment_2 GetPath() const { return mPath; }

    bool direction = true;
    Arrangement_2::Face_const_handle * mFace;
    void SetMoveTo( double progress );
    void SetGoal( Point_2 goal );

    void MoveTo( Point_2 goal );
    void MoveTo( double progress );
    bool Move();
    vector<RobotEventPoint>& GetForwardSequence() { return mForwardEventPoints; }
    vector<RobotEventPoint>& GetBackwardSequence() { return mBackwardEventPoints; }
    void SetForwardSequence( const vector<RobotEventPoint>& fsequence );
    void SetBackwardSequence( const vector<RobotEventPoint>& bsequence );
    void SortSequences();
    void SetGoalRandom( Point_2 goal );
    void SetGoal();
    void ClearAllData();
    void GenerateMedianGoals();
    void ClearExtraData();
    vector< pair< ReflectivePoint, pair< ReflectiveToPathMapPoint, ReflectiveToPathMapPoint > > > mWindowRange;
    friend class Graphics;
    int GetID() const;
    void SetID(int iD);
    double GetProgress() const;
    void RemoveSelfieEventpoints();
};

#endif
