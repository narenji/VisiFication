#include "Robot.h"
#include <GL/glut.h>
#include <iostream>
#include "Utility.h"

Point_2 Robot::GetPosition() const
{
    return mPosition;
}

void Robot::SetPosition(const Point_2 &position)
{
    mPosition = position;
}


Arrangement_2 Robot::GetVisibilityArea() const
{
    return mVisibilityArea;
}

void Robot::SetVisibilityArea(const Arrangement_2 &visibilityArea)
{
    mVisibilityArea = visibilityArea;
}
Robot::Robot(Point_2 position, Segment_2 path)
{

    mPosition = position;
    mPath = path;
//    MoveTo( 5 );        //depends on the scale of discretized segment
}

vector<Window> &Robot::GetWindows()
{
    return mWindows;
}

/**
 * @brief Move robot on its path
 * @return true if finished paces
 * \todo  Change this to move on event points
 */
void Robot::MoveTo( Point_2 goal )
{
    mPosition = goal;
    Vector_2 distanceVec = mPosition - mPath.source() ;
//    mProgress = sqrt ( CGAL::to_double( distanceVec.squared_length())) * 100 / sqrt ( CGAL::to_double(mPath.squared_length() ) );
    //cout<<"Moved to "<<mPosition<<endl;
}

void Robot::MoveTo(double progress )
{
    mProgress = progress;
    Point_2 point = mPath.source() + (mPath.target()-mPath.source()) * mProgress/100;
    MoveTo( point );
}

void Robot::SetMoveTo( double progress )
{
    mProgress = progress;
    Point_2 goal = mPath.source() + (mPath.target()-mPath.source()) * mProgress/10;

    mGoal = goal;
    mReachedGoal = false;
}

void Robot::SetGoal( Point_2 goal )         //TODO
{
    //TODO: Check if the goal is inside the environment, if NO, move the robot to the first intersection point (dest)
    Point_2 dest = goal;

//    if ( GetID() == 0 )
//        cout << mPosition << " , " << dest << endl;
    double step = 0.01;
//    double distX = CGAL::to_double(mPosition.hx() - dest.hx());
//    double distY = CGAL::to_double(mPosition.hy() - dest.hy());
//    dest = Point_2(mPosition.hx() + step * distX, mPosition.hy() + step * distY);
    mPath = Segment_2( mPosition, goal );           //define the new path to traverse

    mGoal = goal;
    mReachedGoal = false;
}

void Robot::SetGoalRandom( Point_2 goall )
{
    double goal = rand() % 101 ;

    mGoal = mPath.source() + (mPath.target()-mPath.source()) * goal/100;
    //cout<< "---- random goal "<<goal<<" % point "<<mGoal<<endl;

//    GetPossibleDest( Segment_2( mPath.source(), mPath.target() ) );

    mReachedGoal = false;
}

Point_2 Robot::GetPossibleDest( Segment_2 potentialPath )
{
//    Utility::IntersectionType interType = Utility::FindIntersection(
//                path, Segment_2( eventitor->mPosition, endOfEPtoPath ), intersectionPlace);
//    Point_2 intersPoint;
//    if( interType == Utility::IT_POINT )
//    {
//        CGAL::assign( intersPoint, intersectionPlace );
//    }
}

bool Robot::Move()
{
    //cout<<"Moving Robot ..."<<endl;
    Point_2 pace;
    if( mMedianGoals.size() > 0 )
    {

        pace = mMedianGoals.front();
        mMedianGoals.erase( mMedianGoals.begin() );
    }
    else
    {
        pace = mGoal;
    }

    MoveTo( pace );

    mReachedGoal = ( mPosition == mGoal );
    //cout<<"Yetishdi?! "<<mReachedGoal<<endl;
    return mReachedGoal;
}

void Robot::SortSequences()
{
    SortSequence( mForwardEventPoints );

    SortSequence( mBackwardEventPoints );

}


int Robot::GetID() const
{
    return mID;
}

void Robot::SetID(int iD)
{
    mID = iD;
}

double Robot::GetProgress() const
{
    return mProgress;
}


void Robot::SortSequence( vector<RobotEventPoint>& sequence )
{
    vector<RobotEventPoint> sortedSequence;
    for( vector<RobotEventPoint>::iterator evenit = sequence.begin();
         evenit != sequence.end(); evenit++ )
    {
        vector<RobotEventPoint>::iterator sortedEvenit = sortedSequence.begin();
        for( ;sortedEvenit != sortedSequence.end(); sortedEvenit++ )
        {
            Vector_2 cur_ep2StartVect( evenit->mMappedOnPathPoint -
                                       mPosition );
            Kernel::FT curDistFormStartSq = cur_ep2StartVect.squared_length();

            Vector_2 sort_ep2StartVect( sortedEvenit->mMappedOnPathPoint -
                                        mPosition );
            Kernel::FT sortDistFormStartSq = sort_ep2StartVect.squared_length();

            if( sortDistFormStartSq > curDistFormStartSq )
            {
                break;
            }

        }
        sortedSequence.insert( sortedEvenit, *evenit );
    }
    //    cout<<"list\n";
    //    for( vector<RobotEventPoint>::iterator evenit = sortedSequence.begin();
    //         evenit != sortedSequence.end(); evenit++ )
    //    {
    //        cout<<"++++++++++++++ sorted "<<evenit->mMappedOnPathPoint<<endl;
    //    }
    sequence = sortedSequence;
}

void Robot::ClearAllData()
{
    mForwardEventPoints.clear();
    mBackwardEventPoints.clear();
    mInternalEventPoints.clear();
    ClearExtraData();

}
void Robot::ClearExtraData()
{
    mWindows.clear();
    mVisibilityArea.clear();
}

void Robot::GenerateMedianGoals()
{
    mMedianGoals.clear();
    if( mGoal != mPosition )
    {
        Utility::PointPlacementType pt = Utility::CheckPointPlacement( mGoal, mPosition, mPath.target() );
//        cout<<mGoal<<" "<< mPosition<<" "<< mPath.target()<<": "<<pt<<endl;
        if( pt == Utility::PPT_BEFORE_FIRST )
        {
            for( vector<RobotEventPoint>::iterator evenit = mBackwardEventPoints.begin();
                 evenit != mBackwardEventPoints.end(); evenit++ )
            {
                if( Utility::CheckPointPlacement( evenit->mMappedOnPathPoint,
                                         mPosition, mGoal ) != Utility::PPT_BETWEEN )
                {
                    break;
                }
                mMedianGoals.push_back( evenit->mMappedOnPathPoint );
            }
        }
        else if( pt == Utility::PPT_BETWEEN )
        {
            for( vector<RobotEventPoint>::iterator evenit = mForwardEventPoints.begin();
                 evenit != mForwardEventPoints.end(); evenit++ )
            {
                if( Utility::CheckPointPlacement( evenit->mMappedOnPathPoint,
                                         mPosition, mGoal ) != Utility::PPT_BETWEEN )
                {
                    break;
                }
                mMedianGoals.push_back( evenit->mMappedOnPathPoint );
            }
        }
        else
        {
            cout<<" GenerateMedianGoals: A bug Found!\n";
            exit( 3 );
        }


    }
}

void Robot::SetForwardSequence( const vector<RobotEventPoint>& fsequence )
{
    mForwardEventPoints = fsequence;
}

void Robot::SetBackwardSequence( const vector<RobotEventPoint>& bsequence )
{
    mBackwardEventPoints = bsequence;
}

void Robot::RemoveSelfieEventpoints( vector<RobotEventPoint>& eventpoints )
{
    for( vector<RobotEventPoint>::iterator evenit = eventpoints.begin();
         evenit != eventpoints.end();  )
    {
        if( evenit->ContainsEdgeWithStart( mID ) )
        {
            eventpoints.erase( evenit );
        }
        else
        {
            evenit++;
        }
    }
}

void Robot::RemoveSelfieEventpoints( )
{
    RemoveSelfieEventpoints( mForwardEventPoints );
    RemoveSelfieEventpoints( mBackwardEventPoints );
    RemoveSelfieEventpoints( mInternalEventPoints );
}
