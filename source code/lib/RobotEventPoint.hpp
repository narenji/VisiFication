#ifndef _ROBOT_EVENTPOINT_H
#define _ROBOT_EVENTPOINT_H
#include "EventPoint.hpp"
#include "ReflectivePoint.hpp"
#include "CGALHelper.hpp"

using namespace std;

class RobotEventPoint: public EventPoint
{
public:
    RobotEventPoint( EventPoint& eventPoint ): EventPoint( eventPoint ) {}
    //Kernel::FT mAngle;
    //Kernel::FT mSquaredDistance;
    ReflectivePoint mCorrespondingReflective;
    Point_2 mMappedOnPathPoint;

};

#endif
