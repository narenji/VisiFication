﻿#include "Visifier.h"
#include "Utility.h"
#include <sstream>
#include "Window.hpp"

using namespace std;

Visifier::Visifier()
{
    
}

bool Visifier::Initialize(string worldData )
{
    //    mStartTime = time( 0 );
    //    TestFunction();
    bool result = true;

    depth = 0;          //related to the depth of each individual AcitiveInitSates

    result = LoadData( worldData );
    if(result)
    {
        CGAL::Bbox_2 boundingBox = mEnvironmentPolygon.bbox();
        mBoundingBoxDiameter = sqrt( pow( (boundingBox.xmax()-boundingBox.xmin()), 2 ) +
                                     pow( (boundingBox.ymax()-boundingBox.ymin()), 2 ) );
        mBoundingBoxCenter.first = (boundingBox.xmax() + boundingBox.xmin())/2;
        mBoundingBoxCenter.second = (boundingBox.ymax() + boundingBox.ymin())/2;
        
        Arrangement_2::Vertex_iterator vit;
        
        int index = 0;
        for (vit = mEnvironment.vertices_begin();
             vit != mEnvironment.vertices_end(); ++vit)
        {
            //cout<<endl<<"New Index "<<index<<" for "<<vit->point();
            mPointToIndexMap[pair<Kernel::FT, Kernel::FT>(vit->point().x(),vit->point().y())] = index;
            index++;
        }
        
        Edge_const_iterator eit;
        int edgeIndex = 0;
        for (eit = mEnvironment.edges_begin(); eit != mEnvironment.edges_end(); ++eit)
        {
            pair<Kernel::FT, Kernel::FT> source(eit->source()->point().x(),
                                                eit->source()->point().y());
            pair<Kernel::FT, Kernel::FT> target(eit->target()->point().x(),
                                                eit->target()->point().y());
            //            cout<<endl<<"New Edge Index "<<edgeIndex<<" for "<<eit->source()->point() <<
            //                  " to "<<eit->target()->point() ;
            mEdgeToIndexMap[pair< pair<Kernel::FT, Kernel::FT>,
                    pair<Kernel::FT, Kernel::FT>> (source, target)] = edgeIndex++;
        }
        
        FindAllReflectives();
        for( int robotIndex = 0; robotIndex < mRobots.size(); robotIndex++ )
        {
            mRobots[robotIndex].SetID( robotIndex );
            DetermineRobotWindowRanges( robotIndex );
        }
        
    }
    
    return result;
}


void Visifier::RefreshMedianGoals( int activeRobotIndex )
{
    mRobots[activeRobotIndex].GenerateMedianGoals();
}

int Visifier::VertexToIndex( Point_2 vertex )
{
    int index = -1;
    map< pair<Kernel::FT, Kernel::FT>, int>::iterator iter =
            mPointToIndexMap.find( pair<Kernel::FT, Kernel::FT>( vertex.x(), vertex.y() ) );
    if( iter != mPointToIndexMap.end() )
    {
        index = iter->second;
        
    }
    return index;
}
Graph *Visifier::GetDualGraph() const
{
    return mDualGraph;
}

void Visifier::SetDualGraph(Graph *dualGraph)
{
    mDualGraph = dualGraph;
}

void Visifier::DFSUtil(int v, vector<vector<int>> adj)
{
    // Mark the current node as visited and print it
    visited[v] = true;

    // Recur for all the vertices adjacent to this vertex


    vector< vector<int> >::iterator row;
    vector<int>::iterator col;

    for (col = adj[v].begin(); col != adj[v].end(); col++)
    {
        if (!visited[*col])
        {
//            cout << i << " connected to " << *col << endl;
            DFSUtil(*col, adj);
        }
    }
}

bool Visifier::IsConnected()
{
   // bool visited[ mRobots.size() ];

    //list<int> adj[] = new std::list<int>[mRobots.size()];
    vector<vector<int>> adj(mRobots.size());

    for( int robotIndex1 = 0; robotIndex1 < mRobots.size(); robotIndex1++ )
    {
        for( int robotIndex2 = 0; robotIndex2 < mRobots.size(); robotIndex2++ )
        {
            if ( AreConnectable(GetRobot(robotIndex1)->mPosition, GetRobot(robotIndex2)->mPosition) )
            {
                adj[robotIndex1].push_back(robotIndex2);    //r2 is in the adjacent list of r1
//                cout << robotIndex1 << " sees " << robotIndex2 << endl;
            }
        }
    }
    for (int i = 0; i < mRobots.size(); i++)
    {
            visited[i] = false;
    }


    DFSUtil(0, adj);
    for (int i = 0; i < mRobots.size(); i++)
    {
            if (visited[i] == false)
            {
                return false;
            }
    }
    return true;
}

bool Visifier::IsCovered()
{
    std::set<int> visibleVertices;
    for(int robotIndex = 0; robotIndex < mRobots.size(); robotIndex++)
    {
        for( int vertexIndex = 0; vertexIndex < mEnvironmentPolygon.size(); vertexIndex++ )
        {
            if ( AreConnectable(mEnvironmentPolygon.vertex(vertexIndex), GetRobot(robotIndex)->mPosition) )
            {
                visibleVertices.insert(vertexIndex);
//                cout << vertexIndex << endl;
            }
        }
    }


//    for (set<int>::iterator i = visibleVertices.begin(); i != visibleVertices.end(); i++)
//    {
//        cout << (*i) << endl;
//    }
//    cout << "--------------------------------------------------" << endl;

    if ( visibleVertices.size() == mEnvironmentPolygon.size() )
    {
        return true;
    }
    return false;
}

void Visifier::ComputeRobotsVisibilityArea()
{
   mWindowIDMap.clear();
   mInternalWindowIDMap.clear();

   Arrangement_2::Face_const_handle * face;
   CGAL::Arr_naive_point_location<Arrangement_2> pl(mEnvironment);

   for(int robotIndex = 0; robotIndex < mRobots.size(); robotIndex++)
   {
       CGAL::Arr_point_location_result<Arrangement_2>::Type obj = pl.locate(mRobots[robotIndex].GetPosition());
       // The query point locates in the interior of a face
       face = boost::get<Arrangement_2::Face_const_handle> (&obj);

       // Define visibiliy object type that computes regularized visibility area
       typedef CGAL::Rotational_sweep_visibility_2<Arrangement_2, CGAL::Tag_true> RSPV;
       Arrangement_2 regular_output;
       RSPV regular_visibility(mEnvironment);

    //        cout << mRobots[robotIndex].GetPosition() << "     " << path << endl;
       regular_visibility.compute_visibility(mRobots[robotIndex].GetPosition(), *face, regular_output);

       SetRobotWindow( robotIndex, regular_output);
    }
}

Arrangement_2 Visifier::GetInterNodeVisibilityArea( Point_2 interNodeLocation )
{

//    cout << interNodeLocation << endl;

    Arrangement_2::Face_const_handle * face;
    CGAL::Arr_naive_point_location<Arrangement_2> pl(mEnvironment);


    CGAL::Arr_point_location_result<Arrangement_2>::Type obj = pl.locate(interNodeLocation);
    // The query point locates in the interior of a face
    face = boost::get<Arrangement_2::Face_const_handle> (&obj);

    // Define visibiliy object type that computes regularized visibility area
    typedef CGAL::Simple_polygon_visibility_2<Arrangement_2, CGAL::Tag_true> RSPV;
    typedef CGAL::Triangular_expansion_visibility_2<Arrangement_2> TEV;
    Arrangement_2 regular_output;
    RSPV regular_visibility(mEnvironment);

//        cout << mRobots[robotIndex].GetPosition() << "     " << path << endl;
    regular_visibility.compute_visibility(interNodeLocation, *face, regular_output);

    return regular_output;
}


Arrangement_2 Visifier::ComputeVisibilityPolygon( Arrangement_2 env, Point_2 query )
{
    Arrangement_2::Face_const_handle * face;
    CGAL::Arr_naive_point_location<Arrangement_2> pl(env);

    CGAL::Arr_point_location_result<Arrangement_2>::Type obj = pl.locate(query);
    // The query point locates in the interior of a face
    face = boost::get<Arrangement_2::Face_const_handle> (&obj);

    // Define visibiliy object type that computes regularized visibility area
    typedef CGAL::Rotational_sweep_visibility_2<Arrangement_2, CGAL::Tag_true> RSPV;
    Arrangement_2 regular_output;
    RSPV regular_visibility(env);

 //        cout << mRobots[robotIndex].GetPosition() << "     " << path << endl;
    regular_visibility.compute_visibility(query, *face, regular_output);

    return regular_output;
}


//bool Visifier::AreVisible(int robotIndex1, int robotIndex2)
//{
//    bool areVisible = true;
//    Arrangement_2::Face_const_handle * face;
//    CGAL::Arr_naive_point_location<Arrangement_2> pl1(mEnvironment);

//    CGAL::Arr_point_location_result<Arrangement_2>::Type obj1 = pl1.locate(mRobots[robotIndex1].GetPosition());
//    // The query point locates in the interior of a face
//    face = boost::get<Arrangement_2::Face_const_handle> (&obj1);

//    // Define visibiliy object type that computes regularized visibility area
//    typedef CGAL::Simple_polygon_visibility_2<Arrangement_2, CGAL::Tag_true> RSPV;
//    Arrangement_2 regular_output;
//    RSPV regular_visibility(mEnvironment);
//    regular_visibility.compute_visibility(mRobots[robotIndex1].GetPosition(), *face, regular_output);

//    CGAL::Arr_naive_point_location<Arrangement_2> pl2(regular_output);
//    CGAL::Arr_point_location_result<Arrangement_2>::Type obj2 = pl2.locate(mRobots[robotIndex2].GetPosition());

//    face = boost::get<Arrangement_2::Face_const_handle> (&obj2);
//    if( (*face)-> is_unbounded() )      //lies in unbounded face
//    {
//        areVisible = false;
//    }

//    return areVisible;
//}

/// \todo: Use map to find out if an edge is window
bool Visifier::IsWindow( Segment_2 edge)
{
    bool result = true;
    Edge_const_iterator eit;
    
    for (eit = mEnvironment.edges_begin(); eit != mEnvironment.edges_end(); ++eit)
    {
        Segment_2 alaki (eit->source()->point(),eit->target()->point());
        if( alaki.has_on( edge.source() ) &&
                alaki.has_on( edge.target() )  )
        {
            result = false;
            break;
        }
    }
    
    return result;
}

void Visifier::SetRobotWindow( int robotIndex, Arrangement_2& visibilityArea )
{
    Edge_iterator eit;
    mRobots[robotIndex].GetWindows().clear();
    
    for (eit = visibilityArea.edges_begin(); eit != visibilityArea.edges_end(); ++eit)
    {
        if( IsWindow( Segment_2( eit->source()->point(),eit->target()->point() ) ))
        {
            
            pair<Kernel::FT, Kernel::FT> source(eit->source()->point().x(),
                                                eit->source()->point().y());
            pair<Kernel::FT, Kernel::FT> target(eit->target()->point().x(),
                                                eit->target()->point().y());
            
            int vertexIndex = -1;
            Segment_2 robotoSource( mRobots[robotIndex].GetPosition(), eit->source()->point());
            Segment_2 robotoTarget( mRobots[robotIndex].GetPosition(), eit->target()->point());
            
            
            if( robotoSource.squared_length() < robotoTarget.squared_length()) //Source point is reflective vertex
            {
                vertexIndex = VertexToIndex(eit->source()->point());
                Window window(eit->source()->point(),eit->target()->point());
                window.mID = pair<int , int >(robotIndex, vertexIndex);
                mRobots[robotIndex].GetWindows().push_back( window );
            }
            else //Target point is reflective
            {
                vertexIndex = VertexToIndex(eit->target()->point());
                Window window(eit->target()->point(), eit->source()->point());
                window.mID = pair<int , int >(robotIndex, vertexIndex);
                mRobots[robotIndex].GetWindows().push_back( window );
            }

//            cout << "Robot's window: " << eit->target()->point() << " " << eit->source()->point() << endl;

            mWindowIDMap[pair< pair<Kernel::FT, Kernel::FT>,
                    pair<Kernel::FT, Kernel::FT>> (source, target)] = pair<int , int >(robotIndex, vertexIndex);
            //Add a reversed key to map for faster search
            mWindowIDMap[pair< pair<Kernel::FT, Kernel::FT>,
                    pair<Kernel::FT, Kernel::FT>> (target, source)] = pair<int , int >(robotIndex, vertexIndex);
        }
        else
        {
        }
    }
    
    mRobots[robotIndex].SetVisibilityArea( visibilityArea );
}

void Visifier::SetInterNodeWindow( Point_2 interNodeLocation, pair<int,int> srcEdgeID, pair<int,int> destEdgeID, Arrangement_2 visibilityArea )
{
    Edge_iterator eit;
//    vector<pair<int,int>> internalNodeID;
//    internalNodeID.push_back(srcEdgeID);
//    internalNodeID.push_back(destEdgeID);

//    mRobots[robotIndex].GetWindows().clear();



    for (eit = visibilityArea.edges_begin(); eit != visibilityArea.edges_end(); ++eit)
    {
        if( IsWindow( Segment_2( eit->source()->point(),eit->target()->point() ) ))
        {

            pair<Kernel::FT, Kernel::FT> source(eit->source()->point().x(),
                                                eit->source()->point().y());
            pair<Kernel::FT, Kernel::FT> target(eit->target()->point().x(),
                                                eit->target()->point().y());

            int vertexIndex = -1;
            Segment_2 robotoSource( interNodeLocation, eit->source()->point());
            Segment_2 robotoTarget( interNodeLocation, eit->target()->point());


            if( robotoSource.squared_length() < robotoTarget.squared_length()) //Source point is reflective vertex
            {
                vertexIndex = VertexToIndex(eit->source()->point());
//                Window window(eit->source()->point(),eit->target()->point());
//                window.mID = pair<int , int >(robotIndex, vertexIndex);
//                mRobots[robotIndex].GetWindows().push_back( window );
            }
            else //Target point is reflective
            {
                vertexIndex = VertexToIndex(eit->target()->point());
//                Window window(eit->target()->point(), eit->source()->point());
//                window.mID = pair<int , int >(robotIndex, vertexIndex);
//                mRobots[robotIndex].GetWindows().push_back( window );
            }
//            cout << srcEdgeID.first << "    " << srcEdgeID.second << "  " << vertexIndex << endl;
//            mInternalWindowIDMap[pair< pair<Kernel::FT, Kernel::FT>,
//                    pair<Kernel::FT, Kernel::FT>> (source, target)] = pair< pair< pair< int, int >, pair< int, int > >, int >(make_pair(srcEdgeID, destEdgeID), vertexIndex);
//            //Add a reversed key to map for faster search
//            mInternalWindowIDMap[pair< pair<Kernel::FT, Kernel::FT>,
//                    pair<Kernel::FT, Kernel::FT>> (target, source)] = pair<pair< pair< int, int >, pair< int, int > >, int >(make_pair(destEdgeID, srcEdgeID), vertexIndex);
        }
        else
        {
        }
    }

//    mRobots[robotIndex].SetVisibilityArea( visibilityArea );
}

void Visifier::ConstructWindowdEnvironment()
{
    //Create windowed environment to detect cells
    mWindowdEnvironment = mEnvironment;
    for(vector<Robot>::iterator robit = mRobots.begin(); robit != mRobots.end(); robit++)
    {
        vector<Window> windows = robit->GetWindows();
        for (vector<Window>::iterator eit = windows.begin(); eit != windows.end(); ++eit)
        {
            CGAL::insert(mWindowdEnvironment,
                         Segment_2(eit->source(), eit->target()));
        }
    }
}

void Visifier::PrintEnvironmentHalfedges( )
{
//    cout << "------------------------------------------------------" << endl;

    Arrangement_2::Face_iterator              fit;
    Arrangement_2::Ccb_halfedge_circulator    curr;
    Arrangement_2 env = mEnvironment;

    for (fit = env.faces_begin(); fit != env.faces_end(); ++fit)
    {

        if (! fit->is_unbounded())
        {
            curr = fit->outer_ccb();
            do
            {
//                cout << curr->source()->point() << "    " << curr->target()->point() << endl;

//                pair<int,int> edgeID = GetEdgeID(Segment_2(curr->source()->point() , curr->target()->point()));
                cout << curr->source()->point() << "    " << curr->target()->point() << endl;
//                cout << edgeID.first << " " << edgeID.second << endl;
//                if ( edgeID.first == -1 && edgeID.second == -1 )
//                {
//                    pair<pair< pair< int, int >, pair< int, int > >, int > edgeIDD = GetInternalEdgeID(Segment_2(curr->source()->point() , curr->target()->point()));
//                    if ( edgeIDD.first.first.first != 0  && edgeIDD.second != -1 )
//                    {
//                        cout << edgeIDD.first.first.first << " " << edgeIDD.second << endl;
//                        cout << "------------------------------------------------------" << endl;
//                    }
//                }
//                cout << "*********" << endl;

                ++curr;
            }
            while (curr != fit->outer_ccb());
        }

    }

}

void Visifier::ComputeInternalNodesVisibilityArea()
{
    Arrangement_2::Face_iterator              fit;
    Arrangement_2::Ccb_halfedge_circulator    curr;

    for (fit = mWindowdEnvironment.faces_begin(); fit != mWindowdEnvironment.faces_end(); ++fit)
    {
        if (! fit->is_unbounded())
        {
            curr = fit->outer_ccb();
            do
            {
                pair<int,int> edgeIDSource = GetEdgeID(Segment_2(curr->source()->point() , curr->target()->point()));
                ++curr;
                if( curr != fit->outer_ccb() )
                {
                    pair<int,int> edgeIDDest = GetEdgeID(Segment_2(curr->source()->point() , curr->target()->point()));
                    if ( !IsOnTheBoundary(curr->source()->point()) )
                    {
//                        --curr;
//                        cout << curr->source()->point() << "    " << curr->target()->point() << endl;
//                        ++curr;
//                        cout << curr->source()->point() << "    " << curr->target()->point() << endl;
//                        cout << edgeIDSource.first << " " << edgeIDSource.second << "----" << edgeIDDest.first << " " << edgeIDDest.second << endl;

//                        cout << "*****************************************" << endl;

//                        --curr;
//                        edgeIDSource = GetEdgeID(Segment_2(curr->target()->point() , curr->source()->point()));
//                        cout << curr->target()->point() << "    " << curr->source()->point() << endl;
//                        ++curr;
//                        edgeIDDest = GetEdgeID(Segment_2(curr->target()->point() , curr->source()->point()));
//                        cout << curr->target()->point() << "    " << curr->source()->point() << endl;
//                        cout << edgeIDSource.first << " " << edgeIDSource.second << "----" << edgeIDDest.first << " " << edgeIDDest.second << endl;


                        SetInterNodeWindow(curr->source()->point(), edgeIDSource, edgeIDDest, GetInterNodeVisibilityArea(curr->source()->point()));
                    }
                }
            }
            while (curr != fit->outer_ccb());
        }

    }

}




void Visifier::ComputeDualGraph( bool computeNodeRepresentative )
{
    //cout<<"Computing dual graph ... "<<endl;


    //Find neighbor of each edge in windowed environment
    map< pair<int, int>, vector<int> > dualTemp;
    Arrangement_2::Face_iterator              fit;
    Arrangement_2::Ccb_halfedge_circulator    curr;
    int                                       faceCounter = 0;
    //Create a map to find index of points in windowed environment
    Arrangement_2::Vertex_iterator vit;
    map< pair<Kernel::FT, Kernel::FT>, int> windowedVertexIndexMap;
    map< int, Point_2> windowedInverseVertexIndexMap;
    int index = 0;

    for (vit = mSubEnv.vertices_begin();
         vit != mSubEnv.vertices_end(); ++vit)
    {
        windowedVertexIndexMap[pair<Kernel::FT, Kernel::FT>(vit->point().x(),vit->point().y())] = index;
        windowedInverseVertexIndexMap[index] = vit->point();
        index++;
    }
    if( mDualGraph != nullptr )
    {
        delete mDualGraph;
        mDualGraph = nullptr;
    }
    mDualGraph = new Graph( mSubEnv.number_of_faces() - 1 );
    for (fit = mSubEnv.faces_begin(); fit != mSubEnv.faces_end(); ++fit)
    {
        if (! fit->is_unbounded())
        {
            
            curr = fit->outer_ccb();
            vector<Segment_2> representativeEdges;
            //cout<<"Info of cell "<<faceCounter<<endl;
            GraphNode* nodes = mDualGraph->GetNodes();
            vector<pair<pair< pair< int, int >, pair< int, int > >, int >>& cellInfo =  nodes[faceCounter].mCellInfo;
            
            do
            {
                pair<int,int> edgeID = GetEdgeID(Segment_2(curr->source()->point() , curr->target()->point()));
                if(  edgeID.second > -1 )
                {
                    cellInfo.push_back( make_pair(make_pair(edgeID, make_pair(-2,-2)), -2) );
                    if( computeNodeRepresentative )
                    {
                        if( representativeEdges.size() < 2 )
                        {
                            representativeEdges.push_back(
                                        Segment_2(curr->source()->point() , curr->target()->point()) );
                        }
                    }
                    //cout<<"< "<<edgeID.first<<", "<<edgeID.second<<" >"<<endl;
                }
                else if ( edgeID.first == -1 && edgeID.second == -1 )   //belongs to subenv edges
                {
                    pair<pair< pair< int, int >, pair< int, int > >, int > internalEdgeID = GetInternalEdgeID(Segment_2(curr->source()->point() , curr->target()->point()));
//                    cout << curr->source()->point() << "    " << curr->target()->point() << endl;
//                    cout << "HEY " << internalEdgeID.first.first.first << "  " << internalEdgeID.first.first.second << "  /  " << internalEdgeID.first.second.first << "  " << internalEdgeID.first.second.second << "  " << internalEdgeID.second << endl;
                    cellInfo.push_back( internalEdgeID );
                    if( computeNodeRepresentative )
                    {
                        if( representativeEdges.size() < 2 )
                        {
                            representativeEdges.push_back(
                                        Segment_2(curr->source()->point() , curr->target()->point()) );
                        }
                    }

                }
                int from = windowedVertexIndexMap[pair<Kernel::FT, Kernel::FT>(curr->source()->point().x(),
                                                                               curr->source()->point().y())];
                int to = windowedVertexIndexMap[pair<Kernel::FT, Kernel::FT>(curr->target()->point().x(),
                                                                             curr->target()->point().y())];
                if(to < from)
                {
                    int temp = from;
                    from = to;
                    to = temp;
                }
                vector<int>& edgeFaceNeighbors = dualTemp[pair<int,int>(from, to)];
                if(std::find(edgeFaceNeighbors.begin(),
                             edgeFaceNeighbors.end(), faceCounter) == edgeFaceNeighbors.end())
                {
                    edgeFaceNeighbors.push_back(faceCounter);
                }
                ++curr;
            }
            while (curr != fit->outer_ccb());
            if( computeNodeRepresentative )
            {
                Point_2 rep( representativeEdges[0].source().x() + representativeEdges[0].target().x() +
                        representativeEdges[1].source().x() + representativeEdges[1].target().x(),
                        representativeEdges[0].source().y() + representativeEdges[0].target().y() +
                        representativeEdges[1].source().y() + representativeEdges[1].target().y());
                nodes[faceCounter].mRepresentative = Point_2( rep.x()/4, rep.y()/4);
                
            }
            faceCounter++;
        }
    }
    
    //Construct dual graph
    
    for(map< pair<int, int>, vector<int> >::iterator dualiter = dualTemp.begin();
        dualiter != dualTemp.end();dualiter++)
    {
        
        vector<int>& edgeFaceNeighbors = dualiter->second;
        if(edgeFaceNeighbors.size() > 1)
        {
            mDualGraph->AddEdge( edgeFaceNeighbors[0], edgeFaceNeighbors[1] );
        }
    }
}
vector<Robot> Visifier::GetRobots() const
{
    return mRobots;
}

double Visifier::Determinan(Point_2 p)
{
    double powX = (pow(CGAL::to_double(p.hx()), 2));
    double powY = (pow(CGAL::to_double(p.hy()), 2));

    return sqrt(powX + powY);
}

void Visifier::MakeRobotDecision( int robotIndex )
{

    Point_2 xUp(0, 0), goal, dist;
    int a = 2, b = 1, c = 1, alpha = 1;
    double xDown = 0;
    for( int robotIndex2 = 0; robotIndex2 < GetRobots().size(); robotIndex2++ )
    {
        if( AreConnectable(GetRobot(robotIndex)->mPosition, GetRobot(robotIndex2)->mPosition) && GetRobot(robotIndex)->mPosition != GetRobot(robotIndex2)->mPosition)//AreVisible(robotIndex, robotIndex2) )
        {
//            cout << "Robot Index1: " << robotIndex << " Robot Index2: " << robotIndex2 << endl;

            Point_2 minusPosY = Point_2(GetRobot(robotIndex)->mPosition.hx() - GetRobot(robotIndex2)->mPosition.hx(), GetRobot(robotIndex)->mPosition.hy() - GetRobot(robotIndex2)->mPosition.hy());

//            cout << GetRobot(robotIndex)->mPosition << "    " << GetRobot(robotIndex2)->mPosition << endl;

            double sqrtPowX = sqrt(pow(CGAL::to_double(minusPosY.hx()), 2) + pow(CGAL::to_double(minusPosY.hy()), 2));
            double y = 1 / pow( sqrtPowX, alpha);

//            cout << "minusPosY: " << minusPosY << endl;
//            cout << "y: " << y << endl;

            double phi = exp(-1 * Determinan(minusPosY) / c);

//            cout << "phi: " << phi << endl;

            double ga = a * (1 - phi);
            double gr = b * phi;
            Point_2 gLeft = Point_2(CGAL::to_double(minusPosY.hx()) / Determinan(minusPosY), CGAL::to_double(minusPosY.hy()) / Determinan(minusPosY));


            double gRight = gr - ga;

//            cout << GetRobot(robotIndex)->mPosition << endl;
//            cout << "ga: " << ga << endl;
//            cout << "gr: " << gr << endl;
//            cout << "gLeft: " << gLeft << endl;
//            cout << "gRight: " << gRight << endl;

            Point_2 g = Point_2(gLeft.hx() * gRight, gLeft.hy() * gRight);

//            cout << "g: " << g << endl;

            xUp = Point_2(xUp.hx() + y * g.hx(), xUp.hy() + y * g.hy());
            xDown = xDown + y;
        }
    }
    Edge_const_iterator eit;
    for (eit = mEnvironment.edges_begin(); eit != mEnvironment.edges_end(); ++eit)
    {
        if( AreConnectable(GetRobot(robotIndex)->mPosition, eit->source()->point()) && GetRobot(robotIndex)->mPosition != eit->source()->point())//AreVisible(robotIndex, robotIndex2) )
        {

                        Point_2 minusPosY = Point_2(GetRobot(robotIndex)->mPosition.hx() - eit->source()->point().hx(), GetRobot(robotIndex)->mPosition.hy() - eit->source()->point().hy());


                        double sqrtPowX = sqrt(pow(CGAL::to_double(minusPosY.hx()), 2) + pow(CGAL::to_double(minusPosY.hy()), 2));
                        double y = 1 / pow(sqrtPowX, alpha);

            //            cout << "minusPosY: " << minusPosY << endl;
            //            cout << "y: " << y << endl;

                        double phi = exp(-1 * Determinan(minusPosY) / c);

            //            cout << "phi: " << phi << endl;

                        double ga = a * (1 - phi);
                        double gr = b * phi;
                        Point_2 gLeft = Point_2(CGAL::to_double(minusPosY.hx()) / Determinan(minusPosY), CGAL::to_double(minusPosY.hy()) / Determinan(minusPosY));


                        double gRight = gr - ga;

            //            cout << GetRobot(robotIndex)->mPosition << endl;
            //            cout << "ga: " << ga << endl;
            //            cout << "gr: " << gr << endl;
            //            cout << "gLeft: " << gLeft << endl;
            //            cout << "gRight: " << gRight << endl;

                        Point_2 g = Point_2(gLeft.hx() * gRight, gLeft.hy() * gRight);

            //            cout << "g: " << g << endl;

                        xUp = Point_2(xUp.hx() + y * g.hx(), xUp.hy() + y * g.hy());
                        xDown = xDown + y;

        }

    }
//    cout << "xUp: " << xUp << endl;
//    cout << "xDown: " << xDown << endl;

    dist = Point_2(CGAL::to_double(xUp.hx()) / xDown, CGAL::to_double(xUp.hy()) / xDown);
    goal = Point_2(GetRobot(robotIndex)->mPosition.hx() + dist.hx(), GetRobot(robotIndex)->mPosition.hy() + dist.hy());

//    cout << "goal: " << goal << endl;
    if (IsOnTheBoundedSide(GetRobot(robotIndex)->mPosition, goal))
    {
//        cout << GetRobot(robotIndex)->mPosition.hx() << "XXXX" << GetRobot(robotIndex)->mPosition.hy() << endl;
//        cout << goal.hx() << "XXXX" << goal.hy() << endl;
        GetRobot(robotIndex)->SetGoal(goal);
//        cout << "Robot " << robotIndex << ": " << GetRobot(robotIndex)->mPosition << " ==> " << goal << endl;
    }
    else
    {
        GetRobot(robotIndex)->SetGoal(GetRobot(robotIndex)->mPosition);
//        cout << "Robot " << robotIndex << ": " << GetRobot(robotIndex)->mPosition << " ==> " << GetRobot(robotIndex)->mPosition << " (" << goal << ")" << endl;

    }





    //mRobots[robotIndex].SetGoalRandom();

//    double alpha = 1;                                        //Alpha algorithm (default action is move forward)
//    int numberOfNeighbours = GetNumberOfVisibleRobots(robotIndex);


//    if ( numberOfNeighbours < alpha )
//    {
//        if ( GetRobot(robotIndex)->direction )          //make a 180 degree turn
//        {
//            GetRobot(robotIndex)->direction = false;
//        }
//        else
//        {
//            GetRobot(robotIndex)->direction = true;
//        }
//    }
//    else if( numberOfNeighbours >= alpha )
//    {
//        int rndDirection = rand() % 2;               //Computes a random turn

//        if( rndDirection == 0 )
//        {
//            GetRobot(robotIndex)->direction = true;
//        }
//        else if( rndDirection == 1 )
//        {
//            GetRobot(robotIndex)->direction = false;

//        }
//    }
//    if ( GetRobot(robotIndex)->direction )          //making final robot's decision
//    {

////        SetMoveRobotForward( robotIndex );
////        GetRobot(robotIndex)->SetGoal( goal );
//    }
//    else
//    {
////        SetMoveRobotBackward( robotIndex );
////        GetRobot(robotIndex)->SetGoal( goal );
//    }

//    Arrangement_2 visibleArea = ComputeVisibilityPolygon(mEnvironment, GetRobot(robotIndex)->mPosition);

//    Edge_const_iterator eit = visibleArea.edges_begin();
//    Point_2 goal_1 = eit->source()->point();
//    Point_2 goal_2 = eit->target()->point();
//    Point_2 goal = Point_2((goal_1.hx() + goal_2.hx()) / 2, (goal_1.hy() + goal_2.hy()) / 2);

//    goal = Point_2(8, 8);
//    int rndDirection = rand() % 2 ;
//    double step = .2;
//    if ( rndDirection == 1 )
//    {
//    if ( IsOnTheBoundedSide(GetRobot(robotIndex)->mPosition, Point_2(GetRobot(robotIndex)->mPosition.hx() + step, GetRobot(robotIndex)->mPosition.hy())))
//        goal = Point_2(GetRobot(robotIndex)->mPosition.hx() + step, GetRobot(robotIndex)->mPosition.hy());
//    else if ( IsOnTheBoundedSide(GetRobot(robotIndex)->mPosition, Point_2(GetRobot(robotIndex)->mPosition.hx() - step, GetRobot(robotIndex)->mPosition.hy())) )
//        goal = Point_2(GetRobot(robotIndex)->mPosition.hx() - step, GetRobot(robotIndex)->mPosition.hy());
//    else if ( IsOnTheBoundedSide(GetRobot(robotIndex)->mPosition, Point_2(GetRobot(robotIndex)->mPosition.hx(), GetRobot(robotIndex)->mPosition.hy() + step)) )
//        goal = Point_2(GetRobot(robotIndex)->mPosition.hx() , GetRobot(robotIndex)->mPosition.hy() + step);
//    else if ( IsOnTheBoundedSide(GetRobot(robotIndex)->mPosition, Point_2(GetRobot(robotIndex)->mPosition.hx(), GetRobot(robotIndex)->mPosition.hy() - step)) )
//        goal = Point_2(GetRobot(robotIndex)->mPosition.hx(), GetRobot(robotIndex)->mPosition.hy() - step);
//    }
//    else
//    {
//    if ( IsOnTheBoundedSide(GetRobot(robotIndex)->mPosition, Point_2(GetRobot(robotIndex)->mPosition.hx(), GetRobot(robotIndex)->mPosition.hy() + step)) )
//        goal = Point_2(GetRobot(robotIndex)->mPosition.hx() , GetRobot(robotIndex)->mPosition.hy() + step);
//    else if ( IsOnTheBoundedSide(GetRobot(robotIndex)->mPosition, Point_2(GetRobot(robotIndex)->mPosition.hx(), GetRobot(robotIndex)->mPosition.hy() - step)) )
//        goal = Point_2(GetRobot(robotIndex)->mPosition.hx(), GetRobot(robotIndex)->mPosition.hy() - step);
//    else if ( IsOnTheBoundedSide(GetRobot(robotIndex)->mPosition, Point_2(GetRobot(robotIndex)->mPosition.hx() + step, GetRobot(robotIndex)->mPosition.hy())) )
//        goal = Point_2(GetRobot(robotIndex)->mPosition.hx() + step, GetRobot(robotIndex)->mPosition.hy());
//    else if ( IsOnTheBoundedSide(GetRobot(robotIndex)->mPosition, Point_2(GetRobot(robotIndex)->mPosition.hx() - step, GetRobot(robotIndex)->mPosition.hy())) )
//        goal = Point_2(GetRobot(robotIndex)->mPosition.hx() - step, GetRobot(robotIndex)->mPosition.hy());
//    }

//    if (!IsSegmentInside(mEnvironment, Segment_2(GetRobot(robotIndex)->mPosition, goal)) )
//    {
//        cout << "Moving segment intersects the polygon" << endl;
//        exit(0);
//    }

////    if ( mEnvironmentPolygon.has_on_unbounded_side(goal) )    //dest must be laid in the visible area of the current robot's position
////    {                                                           //dest's face is unbounded
////        cout << "face is unbounded! (Make Decision process)" << endl;
////        exit(0);
////        //do sth
////    }
////    exit(0);

//    GetRobot(robotIndex)->SetGoal(goal);
}

bool Visifier::IsSegmentInside( Arrangement_2 env, Segment_2 seg )
{
    CGAL::Object intersectionPlace;
    for (Edge_const_iterator eit = env.edges_begin(); eit != env.edges_end(); ++eit)
    {
        Utility::IntersectionType interType = Utility::FindIntersection(seg,
                                                Segment_2(eit->source()->point(), eit->target()->point()),
                                                                        intersectionPlace);
        if ( interType == Utility::IT_POINT || interType == Utility::IT_SEGMENT )
        {
            return false;
        }
    }
    return true;
}

bool Visifier::IsSegmentInsideIrregular( Arrangement_2 env, Segment_2 seg )
{
    CGAL::Object intersectionPlace;
    for (Edge_const_iterator eit = env.edges_begin(); eit != env.edges_end(); ++eit)
    {
        Utility::IntersectionType interType = Utility::FindIntersection(seg,
                                                Segment_2(eit->source()->point(), eit->target()->point()),
                                                                        intersectionPlace);
        if ( interType == Utility::IT_SEGMENT )
        {
            return false;
        }
    }
    return true;
}

int Visifier::GetNumberOfVisibleRobots(int robotIndex)
{
    int numberOfVisibleRobots = -1;

    for( int robotIndex2 = 0; robotIndex2 < GetRobots().size(); robotIndex2++ )
    {
        if( AreConnectable(GetRobot(robotIndex)->mPosition, GetRobot(robotIndex2)->mPosition) )//AreVisible(robotIndex, robotIndex2) )
        {
            numberOfVisibleRobots++;
        }
    }

    return numberOfVisibleRobots;
}

void Visifier::SetMoveRobotForward(int robotIndex)
{
    double progress = GetRobot(robotIndex)->GetProgress() + 1;

    if( progress >= 10 )
    {
        progress = 9;
    }

    GetRobot(robotIndex)->SetMoveTo( progress );

}

void Visifier::SetMoveRobotBackward(int robotIndex)
{
    double progress = GetRobot(robotIndex)->GetProgress() - 1;

    if( progress <= 0 )
    {
        progress = 1;
    }

    GetRobot(robotIndex)->SetMoveTo( progress );
}


bool Visifier::LoadData( string worldData )
{
    bool result = true;
    
    istringstream worldDefs( worldData );
    string line;
    int state = -1;
    std::vector<Point_2> points;
    std::vector<Segment_2> segments;

    while ( getline( worldDefs, line ) )
    {
        istringstream iss(line);
        cout<<line<<endl;
        if(line == "Environment")
        {
            state = 0;
            cout<<"Env State"<<endl;
        }
        else if(line == "Robots")
        {
            state = 1;
            cout<<"robot State"<<endl;
        }
        else if( line == "Holes" )
        {
            state = 2;
            cout << "Env Holes" << endl;
        }
        else if(line != "" && line[0] != '#' )
        {
            float w, x, y, z;
            if (!(iss >> w >> x >> y >> z)) { result = false; break; } // error
//            cout << w << "  " << x << "  " << y << "  " << z << "  " << endl;
            if( state == 0 )
            {

                points.push_back(Point_2( w, x ));
                segments.push_back( Segment_2( Point_2( w, x ), Point_2( y, z ) ) );
            }
            else if( state == 1 )
            {
                //Load robots
                float  pEndX = y, pEndY = z;
                Point_2 rpos ( w, x ) ;
                Point_2 pStart ( w, x ) ;
                Point_2 pEnd ( pEndX ,pEndY ) ;
                mRobots.push_back( Robot( rpos, Segment_2( pStart, pEnd ) ));
            }
        }
        if( state == 2 )
        {

        }
    }
    if(result)
    {
        cout<<"Creating world with "<<points.size()<<" points ... "<<endl;
        for(vector<Point_2>::iterator from = points.begin(); from != points.end(); from++)
        {
            vector<Point_2>::iterator to = from + 1;
            if( to == points.end() )
            {
                to = points.begin();
            }
            mEnvironmentPolygon.push_back( *from );
//            mEnvironmentHolePolygon.add_hole();
        }
        CGAL::insert_non_intersecting_curves( mEnvironment, segments.begin(), segments.end() );

    }
    return result;
}

bool Visifier::MoveRobot( int activeRobotIndex )
{
    return mRobots[activeRobotIndex].Move();
}

int Visifier::EdgeToIndex( Segment_2 edge )
{
    
    pair<Kernel::FT, Kernel::FT> source(edge.source().x(),
                                        edge.source().y());
    pair<Kernel::FT, Kernel::FT> target(edge.target().x(),
                                        edge.target().y());
    return mEdgeToIndexMap[pair< pair<Kernel::FT, Kernel::FT>,
            pair<Kernel::FT, Kernel::FT>> (source, target)];
}

/**
 * @brief Finds Id of edge.
 * @param edge
 * @return If edge is a window , return value will be < Robot's ID, Refelctive VertexID > \
 * If edge is environment's edge, return value will be < -1, Edge's ID in environment >
 */
pair< int, int > Visifier::GetEdgeID( Segment_2 edge)
{
    pair< int, int > edgeID;
    edgeID.first = -1;
    edgeID.second = -1;
    
    
    Edge_const_iterator eit;
    for (eit = mEnvironment.edges_begin(); eit != mEnvironment.edges_end(); ++eit)
    {
        Segment_2 envEdge (eit->source()->point(),eit->target()->point());
        if( envEdge.has_on( edge.source() ) &&
                envEdge.has_on( edge.target() )  )
        {
            edgeID.second = EdgeToIndex( envEdge );
            break;
        }
    }
    
    if( edgeID.second == -1 )
    {
        map< pair <pair<Kernel::FT, Kernel::FT>, pair<Kernel::FT, Kernel::FT> >,
                pair<int,int> >::iterator winIditer = mWindowIDMap.begin();
        for( winIditer; winIditer != mWindowIDMap.end(); winIditer++)
        {
            Segment_2 window (
                        Point_2(winIditer->first.first.first,winIditer->first.first.second),
                        Point_2(winIditer->first.second.first,winIditer->first.second.second) );

            if( window.has_on( edge.source() ) &&
                    window.has_on( edge.target() )  )
            {
                edgeID = winIditer->second;
                break;
            }
        }
    }
    //edgeid.second is equal to -1 iff no matching window is found
    return edgeID;
}

pair< pair< pair< int, int >, pair< int, int > >, int > Visifier::GetInternalEdgeID(Segment_2 edge)
{
    pair< pair< pair< int, int >, pair< int, int > >, int > internalEdgeID;
    internalEdgeID.second = -1;

    map< pair <pair<Kernel::FT, Kernel::FT>, pair<Kernel::FT, Kernel::FT> >,
            pair< pair< pair< int, int >, pair< int, int > >, int > >::iterator winIditer = mInternalWindowIDMap.begin();
    for( winIditer; winIditer != mInternalWindowIDMap.end(); winIditer++)
    {
        Segment_2 window (
                    Point_2(winIditer->first.first.first,winIditer->first.first.second),
                    Point_2(winIditer->first.second.first,winIditer->first.second.second) );
        if( window.has_on( edge.source() ) &&
                window.has_on( edge.target() )  )
        {
            internalEdgeID = winIditer->second;
            break;
        }
    }
    //internalEdgeID.second is equal to -1 iff no matching Internalwindow is found
    return internalEdgeID;
}

bool Visifier::IsOnTheBoundary(Point_2 q)
{
    Arrangement_2 env = mEnvironment;
    bool result = true;
    int numEdgesBefore = env.number_of_edges();
    int numVerticesBefore = env.number_of_vertices();
    CGAL::insert_point(env, q);
    int numEdgesAfter = env.number_of_edges();
    int numVerticesAfter = env.number_of_vertices();
    if( (numEdgesBefore == numEdgesAfter) && (numVerticesBefore + 1 == numVerticesAfter) )
    {
        result = false;
    }
    return result;
}

bool Visifier::IsOnTheBoundedSide(Point_2 s, Point_2 t)
{
    Arrangement_2 env = mEnvironment;
    bool result = false;
    int numEdgesBefore = env.number_of_edges();
    int numVerticesBefore = env.number_of_vertices();
    CGAL::insert(env, Segment_2(s, t));
    int numEdgesAfter = env.number_of_edges();
    int numVerticesAfter = env.number_of_vertices();
    if( (numEdgesBefore + 1 == numEdgesAfter) && (numVerticesBefore + 2 == numVerticesAfter) )
    {
        result = true;
    }
    return result;
}

void Visifier::FindAllEventPoints()
{
    mEventPoints.clear();
    mWindowEventpoints.clear();

//    cout << "-----------------------------------------------------------------" << endl;

    Vertex_const_iterator v;

    Arrangement_2::Face_iterator              fit;
    Arrangement_2::Ccb_halfedge_circulator    curr;
    for (fit = mWindowdEnvironment.faces_begin(); fit != mWindowdEnvironment.faces_end(); ++fit)
    {
        if (! fit->is_unbounded())
        {
            curr = fit->outer_ccb();
//            EventPoint eventPoint;
            do
            {
                EventPoint eventPoint;
                Segment_2 halfEdge(curr->source()->point() , curr->target()->point());
                pair< int, int > edgeID = GetEdgeID( halfEdge );
                pair< int, int > prevEdgeID;
                if( edgeID.first == -1 )           //part of boundary
                {
                    eventPoint.PushEdge( edgeID );
                    eventPoint.mPosition = halfEdge.source();
                    mEventPoints.push_back( eventPoint );
//                    cout << "Part of the boundary: " << curr->source()->point() << " / " << curr->target()->point() << " edgeID: " << edgeID.first << "  " << edgeID.second << endl;
                }
                if( edgeID.first > -1 )     //part of a window
                {
                    if( IsOnTheBoundary(halfEdge.source()) &&
                           IsOnTheBoundary(halfEdge.target()) )    //  a complete window
                    {
                        eventPoint.PushEdge( edgeID );
                        eventPoint.mPosition = halfEdge.source();
                        mEventPoints.push_back( eventPoint );
//                        cout << "A complete window: " << curr->source()->point() << " / " << curr->target()->point() << " edgeID: " << edgeID.first << "  " << edgeID.second << endl;
                    }
                    else if( !IsOnTheBoundary(halfEdge.source()) )    //Source belongs to the internal nodes
                    {
                        eventPoint.PushEdge( edgeID );
//                        cout << "Source belongs to the internal nodes (1): " << curr->source()->point() << " / " << curr->target()->point() << endl;
//                        cout << " edgeID: " << edgeID.first << "  " << edgeID.second << endl;
                        --curr;
                        prevEdgeID = GetEdgeID( Segment_2((curr)->source()->point(), (curr)->target()->point()) );
//                        cout << "Source belongs to the internal nodes (2): " << curr->source()->point() << " / " << curr->target()->point() << endl;
//                        cout << " edgeID: " << prevEdgeID.first << "  " << prevEdgeID.second << endl;
                        ++curr;
                        eventPoint.PushEdge( prevEdgeID );
                        eventPoint.mPosition = halfEdge.source();
                        mEventPoints.push_back( eventPoint );
                    }
                }
                ++curr;
            }
            while (curr != fit->outer_ccb());
        }
    }
}

bool Visifier::AreConnectable( Point_2 point1, Point_2 point2)
{
    bool result = true;
    //cout<<"First Point is "<<point1<<endl;
    //cout<<"Second Point is "<<point2<<endl;
    Segment_2 connectingLine( point1, point2 );
    
    Edge_const_iterator eit;
    for (eit = mEnvironment.edges_begin(); eit != mEnvironment.edges_end(); ++eit)
    {
        Segment_2 edge( eit->source()->point(), eit->target()->point() );
        
        Point_2 ipoint;
        Segment_2 iseg;
        CGAL::Object intersect = CGAL::intersection( connectingLine, edge );
        
        if( CGAL::assign( ipoint, intersect ) )
        {
            if( ipoint != point1 && ipoint != point2 )
            {
                result = false;
                break;
            }
        }
        else if (CGAL::assign(iseg, intersect))
        {
            result = false;
            break;
        }
        
    }
    
    if( result )
    {
        //Check Connecting edge not to be visible just from outside
        Point_2 middlePoint( (point1.x() + point2.x() ) / 2, (point1.y() + point2.y() ) / 2);
        
        //MAKE IT CORRECT LATER
//        if( mEnvironmentPolygon.bounded_side( middlePoint ) == CGAL::ON_UNBOUNDED_SIDE )
//        {
//            result = false;
//        }
    }
    
    return result;
}

void Visifier::CalculateRobotSequences( int robotIndex )
{
    Robot& robot = mRobots[robotIndex];
//    cout << robotIndex << endl;
//    robot.GetBackwardSequence().clear();
//    robot.GetForwardSequence().clear();
    
    vector<RobotEventPoint> fPossibleEventpoints, bPossibleEventpoints;
    
//    PrintFaceHalfEdges(mSubEnv, robotIndex);

    vector< pair< ReflectivePoint, pair< ReflectiveToPathMapPoint, ReflectiveToPathMapPoint > > >::iterator rangit;
    for( rangit = robot.mWindowRange.begin(); rangit != robot.mWindowRange.end(); rangit++ )
    {
        ReflectivePoint reflectivePoint = rangit->first;
        pair< ReflectiveToPathMapPoint, ReflectiveToPathMapPoint > wValiditySegment = rangit->second;
        
        Utility::PointPlacementType pointPlacement = Utility::CheckPointPlacement( robot.mPosition,
                                                                                   wValiditySegment.first.mPosition,
                                                                                           wValiditySegment.second.mPosition);
        //TODO:: to bechecked later => segment_2 params        
//        ComputeInternalRobotBoundaries(reflectivePoint, robotIndex);
//        cout << robot.mPosition << "        " << wValiditySegment.first.mPosition << "   " << wValiditySegment.second.mPosition << endl;
        if( pointPlacement == Utility::PPT_BETWEEN )
        {
            //            cout<<" >>>>>>>>>>>>>> pp between"<<endl;
            Point_2 forwardEndpoint = wValiditySegment.second.mPosition;
            Point_2 backwardEndpoint = wValiditySegment.first.mPosition;

            vector<RobotEventPoint> fEvents = GetFutureEventPoints( reflectivePoint,
                                                                    Segment_2( robot.mPosition, forwardEndpoint ) );
            vector<RobotEventPoint> bEvents = GetFutureEventPoints( reflectivePoint,
                                                                    Segment_2( robot.mPosition, backwardEndpoint ) );
            
            fPossibleEventpoints.insert( fPossibleEventpoints.end(),
                                         fEvents.begin(), fEvents.end() );
            bPossibleEventpoints.insert( bPossibleEventpoints.end(),
                                         bEvents.begin(), bEvents.end() );
                        
        }
        else if( pointPlacement == Utility::PPT_BEFORE_FIRST )
        {
            //            cout<<" >>>>>>>>>>>>>> pp before"<<endl;
            
            vector<RobotEventPoint> fEvents = GetFutureEventPoints( reflectivePoint,
                                                                    Segment_2( wValiditySegment.first.mPosition,
                                                                               wValiditySegment.second.mPosition ) );
            fPossibleEventpoints.insert( fPossibleEventpoints.end(),
                                         fEvents.begin(), fEvents.end() );
        }
        else if( pointPlacement == Utility::PPT_AFTER_SECOND )
        {
            //            cout<<" >>>>>>>>>>>>>> pp after"<<endl;

            vector<RobotEventPoint> bEvents = GetFutureEventPoints( reflectivePoint,
                                                                    Segment_2( wValiditySegment.first.mPosition,
                                                                               wValiditySegment.second.mPosition ) );
            bPossibleEventpoints.insert( bPossibleEventpoints.end(),
                                         bEvents.begin(), bEvents.end() );
        }
        else
        {
//            cout << robot.mPosition << "        " << wValiditySegment.first.mPosition << "   " << wValiditySegment.second.mPosition << endl;
            cout<<" >>>>>>>>>>>>>> Bug 1 in CalculateRobotSequences "<<endl;
            exit( 2 );
        }
    }
    
    robot.SetBackwardSequence( bPossibleEventpoints );
    robot.SetForwardSequence( fPossibleEventpoints );
    robot.RemoveSelfieEventpoints();
    robot.SortSequences();

}



vector<RobotEventPoint> Visifier::GetFutureEventPoints( ReflectivePoint reflectivePoint, Segment_2 path )
{
    vector<RobotEventPoint> possiblePoints;
    mSubEnv = mWindowdEnvironment;
    //Create possible area
    //first point
    Point_2 firstPoint = Utility::ExtendSegmentFromEnd(
                reflectivePoint.mPosition, path.source(), mBoundingBoxDiameter, true );
    //second point
    Point_2 secondPoint = Utility::ExtendSegmentFromEnd(
                reflectivePoint.mPosition, path.target(), mBoundingBoxDiameter, true );
    
    //Find eventpoints positioned on possible area
    for( vector<EventPoint>::iterator eventitor = mEventPoints.begin();
         eventitor != mEventPoints.end(); eventitor++ )
    {
        if( eventitor->mPosition == reflectivePoint.mPosition )
        {
            continue;
        }
        Point_2 endOfEPtoPath = Utility::ExtendSegmentFromEnd(
                    eventitor->mPosition, reflectivePoint.mPosition, mBoundingBoxDiameter, false );
        CGAL::Object intersectionPlace;

//        if( IsSegmentInside(mEnvironment, Segment_2(reflectivePoint.mPosition, endOfEPtoPath)) )
//            cout << reflectivePoint.mPosition << "   " << eventitor->mPosition  << "   " << endOfEPtoPath << endl;
        Utility::IntersectionType interType = Utility::FindIntersection(
                    path, Segment_2( eventitor->mPosition, endOfEPtoPath ), intersectionPlace);
        Point_2 intersPoint;
        if( interType == Utility::IT_POINT )
        {
            CGAL::assign( intersPoint, intersectionPlace );


            if( Utility::CheckInside( eventitor->mPosition, firstPoint,reflectivePoint.mPosition, secondPoint) &&
                    AreConnectable( eventitor->mPosition, reflectivePoint.mPosition ) &&
                    AreConnectable( intersPoint, reflectivePoint.mPosition )
                    )
            {
                RobotEventPoint eventPoint = *eventitor ;
                if( reflectivePoint.mPosition != eventitor->mPosition )
                {
                    Point_2 extEvetToRef = Utility::ExtendSegmentFromEnd( reflectivePoint.mPosition,
                                                                          eventitor->mPosition,
                                                                          mBoundingBoxDiameter, true );
                    if( IsSegmentInside(mEnvironment, Segment_2(reflectivePoint.mPosition, extEvetToRef)) )
                    {
                        CGAL::insert(mSubEnv, Segment_2(reflectivePoint.mPosition, extEvetToRef));
                    }
                    CGAL::Object intersection;
                    Utility::IntersectionType intType = Utility::FindIntersection( path,
                                                                                   Segment_2( reflectivePoint.mPosition,
                                                                                              extEvetToRef), intersection );
                    if( intType == Utility::IT_POINT )
                    {

                        CGAL::assign( eventPoint.mMappedOnPathPoint, intersection );
//                        cout << eventitor->mPosition << "   " << reflectivePoint.mPosition  << "   " << eventPoint.mMappedOnPathPoint << endl;

                        eventPoint.mCorrespondingReflective = reflectivePoint;
                        possiblePoints.push_back( eventPoint );
//                        string str;
//                        eventPoint.ToString( str );
//                        cout<<" ###### "<<str<<endl;
                    }
                }

            }
        }
    }
//    ******************************************
    Arrangement_2::Face_const_handle * face;
    CGAL::Arr_naive_point_location<Arrangement_2> pl(mSubEnv);

//    for( int idx = 0; idx < mRobots.size(); idx++ )
    {
//        TODO: face detection and store boundaries
        CGAL::Arr_point_location_result<Arrangement_2>::Type obj = pl.locate(mRobots[0].mPosition);
        face = boost::get<Arrangement_2::Face_const_handle> (&obj);
        mRobots[0].mFace = face;


//       Arrangement_2::Ccb_halfedge_const_circulator curr = (*face)->outer_ccb();
//       std::cout << "(" << curr->source()->point() << ")";
//       do {
//       Arrangement_2::Halfedge_const_handle he = curr;
//       std::cout << " [" << he->curve() << "] "
//       << "(" << he->target()->point() << ")";
//       } while (++curr != (*face)->outer_ccb());
//       std::cout << std::endl;

       //در بالا، اگر پاره خط اتصالی از ربات به قطعه مربوطه، پاره خط دیگری از محیط را قطع نکند به این معنی است که آن قطعه در سلول ربات مربوطه است
    }
//    cout << "-------------------------------------------" << endl;
//    *****************************************
    return possiblePoints;
}

bool Visifier::IsSelfWindow( vector<EventPoint>::iterator eventitor, Point_2 reflectivePoint )
{
    vector < pair<int, int> > edgeId = eventitor->GetEdges();
    for (unsigned i = 0; i < edgeId.size(); i++)
    {
        if (edgeId.at(i).second == VertexToIndex(reflectivePoint) )
        {
            return true;
        }
    }
    return false;
}

void Visifier::ComputeSubEnv()
{
//    cout << "----------------------------------------" << endl;

    mSubEnv = mWindowdEnvironment;
    Point_2 mappedOnPathPoint;
    map < pair < Point_2, Point_2 >, bool > computedPoints;
    for( map< int, ReflectivePoint >::iterator refit = mReflectivePoints.begin();
         refit != mReflectivePoints.end(); refit++)
    {
        ReflectivePoint reflectivePoint = refit->second;

        //Find eventpoints positioned on possible area
        for( vector<EventPoint>::iterator eventitor = mEventPoints.begin();
             eventitor != mEventPoints.end(); eventitor++ )
        {
            if( eventitor->mPosition == reflectivePoint.mPosition )
            {
                continue;
            }

            RobotEventPoint eventPoint = *eventitor ;
            if( reflectivePoint.mPosition != eventitor->mPosition )
            {
                Point_2 extEvetToRef = Utility::ExtendSegmentFromEnd( eventitor->mPosition,
                                                                      reflectivePoint.mPosition,
                                                                      mBoundingBoxDiameter, false );
                CGAL::Object intersection;
                Edge_const_iterator eit;
                for (eit = mEnvironment.edges_begin(); eit != mEnvironment.edges_end(); ++eit)
                {
                    Segment_2 edge( eit->source()->point(), eit->target()->point() );
                    Utility::IntersectionType intType = Utility::FindIntersection( edge,
                                                                                   Segment_2( reflectivePoint.mPosition,
                                                                                              extEvetToRef), intersection );

                    if( intType == Utility::IT_POINT && AreConnectable( eventitor->mPosition, reflectivePoint.mPosition ) &&
                            !IsSelfWindow(eventitor, reflectivePoint.mPosition) )
                    {
                        CGAL::assign( eventPoint.mMappedOnPathPoint, intersection );
                        if( eventPoint.mMappedOnPathPoint.hx() == 0 )
                        {
                            mappedOnPathPoint = Point_2(0, eventPoint.mMappedOnPathPoint.hy());
                        }
//                        cout << reflectivePoint.mPosition << "  " << eventPoint.mMappedOnPathPoint << endl;
                        if( reflectivePoint.mPosition != eventPoint.mMappedOnPathPoint )
                        {
                            vector<pair<int, int>> edges = eventitor->GetEdges();
                            if ( edges[0].first == 0 || edges[0].first == 1 || edges[0].first == 2 || edges[0].first == 3)
                            {

                                Robot& robot = mRobots[edges[0].first];
//                                cout << reflectivePoint.mPosition << " " << eventPoint.mMappedOnPathPoint << endl;
                                Segment_2 seg( robot.mPosition, eventPoint.mMappedOnPathPoint );
                                if( seg.collinear_has_on(reflectivePoint.mPosition) )
                                {
//                                    cout << "YES" << reflectivePoint.mPosition << " " << eventPoint.mMappedOnPathPoint << endl;
                                    continue;
                                }
                            }
                            if( IsSegmentInsideIrregular(mEnvironment, Segment_2(reflectivePoint.mPosition, eventPoint.mMappedOnPathPoint)) &&
                                    AreConnectable( eventitor->mPosition, eventPoint.mMappedOnPathPoint ) )         //avoid the creation of self windows
                            {
                                map < pair < Point_2, Point_2 >, bool >::iterator itComputedPoints = computedPoints.find(make_pair(eventitor->mPosition, reflectivePoint.mPosition));
                                if ( itComputedPoints == computedPoints.end() )     //new event point
                                {
                                CGAL::insert(mSubEnv, Segment_2(reflectivePoint.mPosition, eventPoint.mMappedOnPathPoint));
                                computedPoints.insert(make_pair(make_pair(eventitor->mPosition, reflectivePoint.mPosition), true));
//                                cout << eventitor->mPosition << "   " << Segment_2(reflectivePoint.mPosition, eventPoint.mMappedOnPathPoint) << endl;

                                vector < pair<int, int> > edgeId = eventitor->GetEdges();
                                pair<Kernel::FT, Kernel::FT> source(reflectivePoint.mPosition.x(),
                                                                    reflectivePoint.mPosition.y());
                                pair<Kernel::FT, Kernel::FT> target(eventPoint.mMappedOnPathPoint.x(),
                                                                    eventPoint.mMappedOnPathPoint.y());

//                                cout << "--------" << edgeId.size() << "---------" << endl;

//                                for (unsigned i = 0; i < edgeId.size(); i++)
//                                {
//                                    cout << edgeId.at(i).first << " " << edgeId.at(i).second << endl;
//                                }

                                if ( edgeId.size() > 1 )
                                {
                                    int vertexIndex = VertexToIndex(reflectivePoint.mPosition);
                                    mInternalWindowIDMap[pair< pair<Kernel::FT, Kernel::FT>,
                                            pair<Kernel::FT, Kernel::FT>> (source, target)] = pair< pair< pair< int, int >, pair< int, int > >, int >(make_pair(edgeId.at(0), edgeId.at(1)), vertexIndex);
                                    //Add a reversed key to map for faster search
                                    mInternalWindowIDMap[pair< pair<Kernel::FT, Kernel::FT>,
                                            pair<Kernel::FT, Kernel::FT>> (target, source)] = pair< pair< pair< int, int >, pair< int, int > >, int >(make_pair(edgeId.at(0), edgeId.at(1)), vertexIndex);

                                }

                            }
//                                cout << eventitor->mPosition << "   " << Segment_2(reflectivePoint.mPosition, mappedOnPathPoint) << endl;

//                                cout << eventPoint.mMappedOnPathPoint << endl;
//                                cout << "Number of Intersected Edges: " << eventitor->GetNumOfEdges() << endl;
                            }
                        }
                    }
                }
            }
//            PrintFaceHalfEdges(mSubEnv, 1);
        }
    }
}

void Visifier::ComputeRobotsSubCells()
{
    Point_2 mappedOnPathPoint;

    for( int robotIndex = 0; robotIndex < mRobots.size(); robotIndex++ )
    {
        Robot& robot = mRobots[robotIndex];
        robot.mInternalEventPoints.clear();
    }

    for( map< int, ReflectivePoint >::iterator refit = mReflectivePoints.begin();
         refit != mReflectivePoints.end(); refit++)
    {
        ReflectivePoint reflectivePoint = refit->second;

        //Find eventpoints positioned on possible area
        for( vector<EventPoint>::iterator eventitor = mEventPoints.begin();
             eventitor != mEventPoints.end(); eventitor++ )
        {
            if( eventitor->mPosition == reflectivePoint.mPosition )
            {
                continue;
            }
            RobotEventPoint eventPoint = *eventitor ;
            if( reflectivePoint.mPosition != eventitor->mPosition )
            {
                Point_2 extEvetToRef = Utility::ExtendSegmentFromEnd( eventitor->mPosition,
                                                                      reflectivePoint.mPosition,
                                                                      mBoundingBoxDiameter, false );
                CGAL::Object intersection;
                Edge_const_iterator eit;
                for (eit = mEnvironment.edges_begin(); eit != mEnvironment.edges_end(); ++eit)
                {
                    Segment_2 edge( eit->source()->point(), eit->target()->point() );
                    Utility::IntersectionType intType = Utility::FindIntersection( edge,
                                                                                   Segment_2( reflectivePoint.mPosition,
                                                                                              extEvetToRef), intersection );
                    if( intType == Utility::IT_POINT )
                    {
                        CGAL::assign( eventPoint.mMappedOnPathPoint, intersection );

                        if( eventPoint.mMappedOnPathPoint.hx() == 0 )
                        {
                            mappedOnPathPoint = Point_2(0, eventPoint.mMappedOnPathPoint.hy());
                        }

//                        cout << eventitor->mPosition << "  " <<reflectivePoint.mPosition << "  " << eventPoint.mMappedOnPathPoint << endl;
                        if( reflectivePoint.mPosition != eventPoint.mMappedOnPathPoint )
                        {
//                            if( reflectivePoint.mPosition == Point_2(2, 0) )
//                            {
//                                cout << Segment_2(reflectivePoint.mPosition, eventPoint.mMappedOnPathPoint) << endl;
//                            }
                            if( IsSegmentInsideIrregular(mEnvironment, Segment_2(reflectivePoint.mPosition, mappedOnPathPoint)) )
                            {
                                for( int robotIndex = 0; robotIndex < mRobots.size(); robotIndex++ )
                                {
                                    Robot& robot = mRobots[robotIndex];
                                    Arrangement_2::Face_const_handle * face;
                                    CGAL::Arr_naive_point_location<Arrangement_2> pl(mSubEnv);
                                    CGAL::Arr_point_location_result<Arrangement_2>::Type obj = pl.locate(robot.mPosition);

                                    // The query point locates in the interior of a face
                                    face = boost::get<Arrangement_2::Face_const_handle> (&obj);
                                    Arrangement_2::Ccb_halfedge_const_circulator   curr;
                                    if (face != nullptr)
                                    {
                                        curr = (*face)->outer_ccb();
                                        Segment_2 subWindow(reflectivePoint.mPosition, eventPoint.mMappedOnPathPoint);
        //                                cout << reflectivePoint.mPosition << "  " << eventPoint.mMappedOnPathPoint << " <=edge" << endl;
                                        do
                                        {
        //                                    cout << curr->source()->point() << "  " << curr->target()->point() << " <=half edges" <<endl;
                                            Segment_2 halfEdge(curr->source()->point() , curr->target()->point());
                                            Point_2 midHalfEdge((halfEdge.source().x() + halfEdge.target().x()) / 2,
                                                                  (halfEdge.source().y() + halfEdge.target().y()) / 2);
//                                            cout << subWindow.collinear_has_on(midHalfEdge) << endl;
                                            bool newEvent = true;
//                                            PrintFaceHalfEdges(mSubEnv, robotIndex);
                                            midHalfEdge = CGAL::midpoint(curr->source()->point() , curr->target()->point());
                                            if( subWindow.source() == subWindow.target() )
                                            {
                                                continue;
                                            }
//                                            cout << "SubWindow: " << subWindow << " / " << halfEdge << endl;

                                            if( CGAL::collinear(subWindow.source(), CGAL::midpoint(halfEdge.source(), halfEdge.target()), subWindow.target()) )                       //sub-cell boundary of robot is found
                                            {
//                                                cout << robotIndex << ": SubWindow: " << subWindow << ", Half Edge: " << halfEdge << endl;
                                                newEvent = true;
                                                for( int index = 0; index < robot.mInternalEventPoints.size(); index++ )
                                                {
                                                    EventPointBase re1 = robot.mInternalEventPoints[index];
                                                    if ( re1 == *eventitor )
                                                    {
                                                        newEvent = false;
                                                    }
                                                }
                                                if( true /*newEvent*/)
                                                {
//                                                    cout << "robot " << robotIndex << ": " << curr->source()->point() << "  " << curr->target()->point() << " <=half edges detected" <<endl;
//                                                    cout << eventitor->mPosition << endl;
                                                    robot.mInternalEventPoints.push_back(*eventitor);
                                                }
                                            }
                                            ++curr;
                                        }
                                        while (curr != (*face)->outer_ccb());
        //                                cout << "**************************" << endl;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

void Visifier::PrintFaceHalfEdges(Arrangement_2 env, int robotIndex)
{
    Robot& robot = mRobots[robotIndex];

    Arrangement_2::Face_const_handle * face;
    CGAL::Arr_naive_point_location<Arrangement_2> pl(env);
    CGAL::Arr_point_location_result<Arrangement_2>::Type obj = pl.locate(robot.mPosition);

    // The query point locates in the interior of a face
    face = boost::get<Arrangement_2::Face_const_handle> (&obj);
    Arrangement_2::Ccb_halfedge_const_circulator   curr;

    if (face != nullptr)
    {
        curr = (*face)->outer_ccb();
        //                                cout << reflectivePoint.mPosition << "  " << eventPoint.mMappedOnPathPoint << " <=edge" << endl;
        cout << "Robot faces: " << robotIndex << "-----" << endl;
        do
        {
        //                                    cout << curr->source()->point() << "  " << curr->target()->point() << " <=half edges" <<endl;
            Segment_2 halfEdge(curr->source()->point() , curr->target()->point());
            cout << curr->source()->point() << "  " << curr->target()->point() << " <=half edges" <<endl;
            ++curr;
        }
        while (curr != (*face)->outer_ccb());
    }
}

bool Visifier::IsReflective( Point_2 firstPoint, Point_2 middlePoint, Point_2 lastPoint )
{
    return Utility::AngleBetweenThreePoints( firstPoint, middlePoint, lastPoint ) > M_PI;
}

void Visifier::FindAllReflectives()
{
    for( int vertexIndex = 0; vertexIndex < mEnvironment.number_of_vertices(); vertexIndex++ )
    {
        int firstIndex = vertexIndex - 1;
        if( firstIndex < 0)
        {
            firstIndex = mEnvironment.number_of_vertices() - 1;
        }
        int middleIndex = vertexIndex;
        int secondIndex = ( vertexIndex + 1 ) % mEnvironment.number_of_vertices();
        
        if( IsReflective( mEnvironmentPolygon.vertex( firstIndex ),
                          mEnvironmentPolygon.vertex( middleIndex ),
                          mEnvironmentPolygon.vertex( secondIndex ) )  )
        {
            ReflectivePoint reflectivePoint;
            reflectivePoint.mIndexInEnvironment = VertexToIndex( mEnvironmentPolygon.vertex( middleIndex ) );
            reflectivePoint.mPosition = mEnvironmentPolygon.vertex( middleIndex );
            reflectivePoint.mFirstEdgeStartCCW =  mEnvironmentPolygon.vertex( firstIndex );
            reflectivePoint.mSecondEdgeEndCCW = mEnvironmentPolygon.vertex( secondIndex );
            reflectivePoint.mFirstEdgeOpposite = Utility::ExtendSegmentFromEnd( reflectivePoint.mPosition,
                                                                                reflectivePoint.mFirstEdgeStartCCW,
                                                                                mBoundingBoxDiameter, true);
            reflectivePoint.mSecondEdgeOpposite = Utility::ExtendSegmentFromEnd( reflectivePoint.mPosition,
                                                                                 reflectivePoint.mSecondEdgeEndCCW,
                                                                                 mBoundingBoxDiameter, true);
            
            reflectivePoint.mFirstEdgeExtendedStartCCW =
                    Utility::ExtendSegmentFromEnd( reflectivePoint.mPosition, reflectivePoint.mFirstEdgeStartCCW,
                                                   mBoundingBoxDiameter, false);
            reflectivePoint.mSecondEdgeExtendedEndCCW =
                    Utility::ExtendSegmentFromEnd( reflectivePoint.mPosition, reflectivePoint.mSecondEdgeEndCCW,
                                                   mBoundingBoxDiameter, false);
            mReflectivePoints[ vertexIndex ] = reflectivePoint;
        }
    }
    
}

void Visifier::DetermineRobotWindowRanges( int robotIndex )
{
    //    cout<<"Determining ranges for robot "<<robotIndex<<endl;
    
    Robot& robot = mRobots[robotIndex];
    robot.mWindowRange.clear();
    for( map< int, ReflectivePoint >::iterator refit = mReflectivePoints.begin();
         refit != mReflectivePoints.end(); refit++)
    {
        ReflectivePoint reflectivePoint = refit->second;
        //        cout<<"reflective "<<reflectivePoint.mPosition;
        
        CGAL::Object firstIntersection, secondIntersection;
        Utility::IntersectionType firstIntersectionType, secondIntersectionType;
        firstIntersectionType = Utility::FindIntersection( robot.mPath,
                                                           Segment_2( reflectivePoint.mPosition,
                                                                      reflectivePoint.mSecondEdgeOpposite ), firstIntersection );
        secondIntersectionType = Utility::FindIntersection( robot.mPath,
                                                            Segment_2( reflectivePoint.mPosition,
                                                                       reflectivePoint.mFirstEdgeOpposite ), secondIntersection );
        //        cout<<"first inters "<<firstIntersectionType<<"sec inters "<<secondIntersectionType<<endl;
        // 0 for path end point means that point is placed at safe point, where window will not be created in that area.
        // -1 or 1 means the end point is placed in one of areas which a window can be created in those areas.
        int pathStartPartNumber = INT_MAX, pathEndPartNumber = INT_MAX;
        
        if( Utility::CheckInside( robot.mPath.source(), reflectivePoint.mFirstEdgeStartCCW, reflectivePoint.mPosition,
                                  reflectivePoint.mSecondEdgeOpposite ) )
        {
            pathStartPartNumber = -1;
        }
        //else is removed for situation where start point is placed on border of first and safe area
        // in such situations safe area is better choice due to easier decision making in next steps.
        /*else*/ if( Utility::CheckInside( robot.mPath.source(), reflectivePoint.mSecondEdgeOpposite, reflectivePoint.mPosition,
                                           reflectivePoint.mFirstEdgeOpposite ) )
        {
            pathStartPartNumber = 0;
        }
        else if( Utility::CheckInside( robot.mPath.source(), reflectivePoint.mFirstEdgeOpposite, reflectivePoint.mPosition,
                                       reflectivePoint.mSecondEdgeEndCCW ) )
        {
            pathStartPartNumber = 1;
        }
        
        if( Utility::CheckInside( robot.mPath.target(), reflectivePoint.mFirstEdgeStartCCW, reflectivePoint.mPosition,
                                  reflectivePoint.mSecondEdgeOpposite ) )
        {
            pathEndPartNumber = -1;
        }
        //else is removed for situation where end point is placed on border of first and safe area
        // in such situations safe area is better choice due to easier decision making in next steps.
        /*else*/ if( Utility::CheckInside( robot.mPath.target(), reflectivePoint.mSecondEdgeOpposite, reflectivePoint.mPosition,
                                           reflectivePoint.mFirstEdgeOpposite ) )
        {
            pathEndPartNumber = 0;
        }
        else if( Utility::CheckInside( robot.mPath.target(), reflectivePoint.mFirstEdgeOpposite, reflectivePoint.mPosition,
                                       reflectivePoint.mSecondEdgeEndCCW ) )
        {
            pathEndPartNumber = 1;
        }
        //        cout<< "Path start number: "<<pathStartPartNumber<<" pathend num: "<<pathEndPartNumber<<endl;
        
        if( pathStartPartNumber == 0 && pathEndPartNumber == 0)
        {
            //This reflective point is not important for us
            continue;
        }
        else if( pathEndPartNumber == pathStartPartNumber)
        {
            //This reflective point is important through all path
            ReflectiveToPathMapPoint firstRef, secondRef;
            firstRef.mPosition = robot.mPath.source();
            secondRef.mPosition = robot.mPath.target();
            pair< ReflectiveToPathMapPoint, ReflectiveToPathMapPoint > segment;
            segment.first = firstRef;
            segment.second = secondRef;
            
            robot.mWindowRange.push_back( pair< ReflectivePoint, pair< ReflectiveToPathMapPoint, ReflectiveToPathMapPoint > >
                                          ( reflectivePoint, segment ) );
        }
        else if( abs( pathStartPartNumber - pathEndPartNumber) == 1 )
        {
            //            cout<<"*************** 1\n";
            Point_2 start, end;
            if( pathStartPartNumber == -1 )
            {
                start = robot.mPath.source();
                if( !CGAL::assign( end, firstIntersection ) )
                {
                    cout<<"########################### 1: A BUG FOUND! [IT = "<<firstIntersectionType<<"]"<<endl;
                    exit(1);
                }
            }
            else if( pathStartPartNumber == 0)
            {
                end = robot.mPath.target();
                if( firstIntersectionType == Utility::IT_POINT )
                {
                    if( !CGAL::assign( start, firstIntersection ) )
                    {
                        cout<<"########################### 2: A BUG FOUND! [IT = "<<firstIntersectionType<<"]"<<endl;
                        exit(1);
                    }
                }
                else if( secondIntersectionType == Utility::IT_POINT )
                {
                    if( !CGAL::assign( start, secondIntersection ) )
                    {
                        cout<<"########################### 3: A BUG FOUND! [IT = "<<secondIntersectionType<<"]"<<endl;
                        exit(1);
                    }
                }
                else
                {
                    cout<<"########################### 4: A BUG FOUND! [IT = "<<firstIntersectionType<<
                          ", "<<secondIntersectionType<<"]"<<endl;
                    exit(1);
                }
                
            }
            else if( pathStartPartNumber == 1)
            {
                start = robot.mPath.source();
                if( !CGAL::assign( end, secondIntersection ) )
                {
                    cout<<"########################### 5: A BUG FOUND! [IT = "<<secondIntersectionType<<"]"<<endl;
                    exit(1);
                }
            }
            
            ReflectiveToPathMapPoint firstRef, secondRef;
            firstRef.mPosition = start;
            secondRef.mPosition = end;
            pair< ReflectiveToPathMapPoint, ReflectiveToPathMapPoint > segment;
            segment.first = firstRef;
            segment.second = secondRef;
            
            robot.mWindowRange.push_back( pair< ReflectivePoint, pair< ReflectiveToPathMapPoint, ReflectiveToPathMapPoint > >
                                          ( reflectivePoint, segment ) );
        }
        //
        else if( abs( pathStartPartNumber - pathEndPartNumber) == 2 )
        {
            //            cout<<"*************** 2\n";
            Point_2 start1, end1, start2, end2;
            if( pathStartPartNumber == -1 )
            {
                if( firstIntersectionType == Utility::IT_POINT )
                {
                    start1 = robot.mPath.source();
                    if( !CGAL::assign( end1, firstIntersection ) )
                    {
                        cout<<"########################### 6: A BUG FOUND! [IT = "<<firstIntersectionType<<"]"<<endl;
                        exit(1);
                    }
                }
                else
                {
                    cout << robot.mPath.source() << "   " << robot.mPath.target() << "  " << reflectivePoint.mPosition << endl;

                    cout<<"########################### 7: A BUG FOUND! [IT = "<<firstIntersectionType<<"]"<<endl;
                    exit(1);
                }
                if( secondIntersectionType == Utility::IT_POINT )
                {
                    end2 = robot.mPath.target();
                    if( !CGAL::assign( start2, secondIntersection ) )
                    {
                        cout<<"########################### 8: A BUG FOUND! [IT = "<<secondIntersectionType<<"]"<<endl;
                        exit(1);
                    }
                }
                else
                {
                    cout<<"########################### 9: A BUG FOUND! [IT = "<<secondIntersectionType<<"]"<<endl;
                    exit(1);
                }
            }
            else //if( pathStartPartNumber == 1 )
            {
                if( secondIntersectionType == Utility::IT_POINT )
                {
                    start1 = robot.mPath.source();
                    
                    if( !CGAL::assign( end1, secondIntersection ) )
                    {
                        cout<<"########################### 10: A BUG FOUND! [IT = "<<secondIntersectionType<<"]"<<endl;
                        exit(1);
                    }
                }
                else
                {
                    cout<<"########################### 11: A BUG FOUND! [IT = "<<secondIntersectionType<<"]"<<endl;
                    exit(1);
                }
                if( firstIntersectionType == Utility::IT_POINT )
                {
                    end2 = robot.mPath.target();
                    if( !CGAL::assign( start2, firstIntersection ) )
                    {
                        cout<<"########################### 12: A BUG FOUND! [IT = "<<firstIntersectionType<<"]"<<endl;
                        exit(1);
                    }
                }
                else
                {
                    cout<<"########################### 13: A BUG FOUND! [IT = "<<firstIntersectionType<<"]"<<endl;
                    exit(1);
                }
            }
            
            ReflectiveToPathMapPoint firstRef1, secondRef1;
            firstRef1.mPosition = start1;
            secondRef1.mPosition = end1;
            pair< ReflectiveToPathMapPoint, ReflectiveToPathMapPoint > segment1;
            segment1.first = firstRef1;
            segment1.second = secondRef1;
            
            robot.mWindowRange.push_back( pair< ReflectivePoint, pair< ReflectiveToPathMapPoint, ReflectiveToPathMapPoint > >
                                          ( reflectivePoint, segment1 ) );
            
            ReflectiveToPathMapPoint firstRef2, secondRef2;
            firstRef2.mPosition = start2;
            secondRef2.mPosition = end2;
            pair< ReflectiveToPathMapPoint, ReflectiveToPathMapPoint > segment2;
            segment2.first = firstRef2;
            segment2.second = secondRef2;
            
            robot.mWindowRange.push_back( pair< ReflectivePoint, pair< ReflectiveToPathMapPoint, ReflectiveToPathMapPoint > >
                                          ( reflectivePoint, segment2 ) );
        }
        else //This means one of end points is placed at CW area of reflective point
        {
            //            cout<<"*************** max int\n";
            Point_2 start, end;
            CGAL::Object firstExtendedIntersection, secondExtendedIntersection;
            Utility::IntersectionType firstExtendedIntersectionType, secondExtendedIntersectionType;
            
            firstExtendedIntersectionType = Utility::FindIntersection( robot.mPath,
                                                                       Segment_2( reflectivePoint.mPosition,
                                                                                  reflectivePoint.mFirstEdgeExtendedStartCCW ),
                                                                       firstExtendedIntersection );
            secondExtendedIntersectionType = Utility::FindIntersection( robot.mPath,
                                                                        Segment_2( reflectivePoint.mPosition,
                                                                                   reflectivePoint.mSecondEdgeExtendedEndCCW),
                                                                        secondExtendedIntersection );
            if( pathEndPartNumber == INT_MAX)
            {// We supposed that environment has not any hole inside
                ///\todo: Change here to consider holes inside environment
                if( pathStartPartNumber == -1 )
                {
                    start = robot.mPath.source();
                    if( !CGAL::assign( end, firstExtendedIntersection ) )
                    {
                        cout<<"########################### 14: A BUG FOUND! [IT = "<<firstExtendedIntersectionType<<"]"<<endl;
                        exit(1);
                    }
                }
                else if( pathStartPartNumber == 0 )
                {
                    if( firstExtendedIntersectionType == Utility::IT_POINT )
                    {
                        if( !CGAL::assign( start, firstIntersection ) )
                        {
                            cout<<"########################### 15: A BUG FOUND! [IT = "<<firstIntersectionType<<"]"<<endl;
                            exit(1);
                        }
                        if( !CGAL::assign( end, firstExtendedIntersection ) )
                        {
                            cout<<"########################### 16: A BUG FOUND! [IT = "<<firstExtendedIntersectionType<<"]"<<endl;
                            exit(1);
                        }
                    }
                    else if( secondExtendedIntersectionType == Utility::IT_POINT )
                    {
                        if( !CGAL::assign( start, secondIntersection ) )
                        {
                            cout<<"########################### 17: A BUG FOUND! [IT = "<<secondIntersectionType<<"]"<<endl;
                            exit(1);
                        }
                        if( !CGAL::assign( end, secondExtendedIntersection ) )
                        {
                            cout<<"########################### 18: A BUG FOUND! [IT = "<<secondExtendedIntersectionType<<"]"<<endl;
                            exit(1);
                        }
                    }
                    else
                    {
                        cout<<"########################### 19: A BUG FOUND! [IT = "<<firstExtendedIntersectionType<<
                              ", "<<secondExtendedIntersectionType<<"]"<<endl;
                        exit(1);
                    }
                }
                else
                {
                    start = robot.mPath.source();
                    if( !CGAL::assign( end, secondExtendedIntersection ) )
                    {
                        cout<<"########################### 20: A BUG FOUND! [IT = "<<secondExtendedIntersectionType<<"]"<<endl;
                        exit(1);
                    }
                }
                ReflectiveToPathMapPoint firstRef, secondRef;
                firstRef.mPosition = start;
                secondRef.mPosition = end;
                pair< ReflectiveToPathMapPoint, ReflectiveToPathMapPoint > segment;
                segment.first = firstRef;
                segment.second = secondRef;
                
                robot.mWindowRange.push_back( pair< ReflectivePoint, pair< ReflectiveToPathMapPoint, ReflectiveToPathMapPoint > >
                                              ( reflectivePoint, segment ) );
            }
            else
            {
                if( pathEndPartNumber == -1 )
                {
                    end = robot.mPath.target();
                    if( !CGAL::assign( start, firstExtendedIntersection ) )
                    {
                        cout<<"########################### 21: A BUG FOUND! [IT = "<<firstExtendedIntersectionType<<"]"<<endl;
                        exit(1);
                    }
                }
                else if( pathEndPartNumber == 0 )
                {
                    if( firstExtendedIntersectionType == Utility::IT_POINT )
                    {
                        if( !CGAL::assign( end, firstIntersection ) )
                        {
                            cout<<"########################### 22: A BUG FOUND! [IT = "<<firstIntersectionType<<"]"<<endl;
                            exit(1);
                        }
                        if( !CGAL::assign( start, firstExtendedIntersection ) )
                        {
                            cout<<"########################### 23: A BUG FOUND! [IT = "<<firstExtendedIntersectionType<<"]"<<endl;
                            exit(1);
                        }
                    }
                    else if( secondExtendedIntersectionType == Utility::IT_POINT )
                    {
                        if( !CGAL::assign( end, secondIntersection ) )
                        {
                            cout<<"########################### 24: A BUG FOUND! [IT = "<<secondIntersectionType<<"]"<<endl;
                            exit(1);
                        }
                        if( !CGAL::assign( start, secondExtendedIntersection ) )
                        {
                            cout<<"########################### 25: A BUG FOUND! [IT = "<<secondExtendedIntersectionType<<"]"<<endl;
                            exit(1);
                        }
                    }
                    else
                    {
                        cout<<"########################### 26: A BUG FOUND! [IT = "<<firstExtendedIntersectionType<<
                              ", "<<secondExtendedIntersectionType<<"]"<<endl;
                        exit(1);
                    }
                }
                else
                {
                    end = robot.mPath.target();
                    if( !CGAL::assign( start, secondExtendedIntersection ) )
                    {
                        cout<<"########################### 27: A BUG FOUND! [IT = "<<secondExtendedIntersectionType<<"]"<<endl;
                        exit(1);
                    }
                }
                ReflectiveToPathMapPoint firstRef, secondRef;
                firstRef.mPosition = start;
                secondRef.mPosition = end;
                pair< ReflectiveToPathMapPoint, ReflectiveToPathMapPoint > segment;
                segment.first = firstRef;
                segment.second = secondRef;
                
                robot.mWindowRange.push_back( pair< ReflectivePoint, pair< ReflectiveToPathMapPoint, ReflectiveToPathMapPoint > >
                                              ( reflectivePoint, segment ) );
            }
            
        }
        
    }
}

Robot* Visifier::GetRobot( int robotIndex )
{
    return &mRobots[robotIndex];
}
