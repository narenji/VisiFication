#ifndef _EVENTPOINT_BASE_H
#define _EVENTPOINT_BASE_H
#include "CGALHelper.hpp"

using namespace std;


class EventPointBase
{
    vector< pair< int, int> > mEdges;
public:

    bool ContainsWindow( pair< int, int> window );
    bool operator==( const EventPointBase& rhs );
    bool operator!=( const EventPointBase& rhs );
    vector<pair<int, int> > GetEdges() const;
    void PushEdge( pair< int, int > edge );
    virtual void ToString( string& output );
    bool ContainsEdgeWithStart( int start );
    int GetNumOfEdges();
    void AddEdges( vector<pair<int, int>> edges );
    vector<pair<int, int>> GetEdges();
    vector< Segment_2 > mHalfEdges;

};


#endif
