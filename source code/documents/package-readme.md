VisiFication 2.0.0
===========================

We implemented a library called __VisiFication__ using Computational Geometry Algorithms Library (CGAL)
in order to automatically
generate a discrete state space of a given robot system. This
allows using existing model checking tools and algorithms. We
construct the state space of a number of robots, each moves freely within a bounded polygonal
area. This state space can then be used to verify visibility
properties (e.g., if the robots can collectively see the entire area).

VisiFication package contains two applications, __HighPerformance__, which is used to achieve the experimental results mentioned in the paper, and __Interactive__, a graphical interface is provided in which a user make the robots' decisions and the new states will be generated respectively.


**VisiFicationInteractive** is an application which the user can use it to see how states are created during the movements of the robots. 
The user can see the environment graphically and select and move robots using this application to see states' information.
The following figure (a snapshot of the Interactive application) shows the forward and the backward sequences for each robot, the subdivision of the environment and the corresponding dual graph.



![VisiFicationInteractive Snapshot][VisiFicationInteractive]

[VisiFicationInteractive]: https://gitlab.com/mayssamin/VisiFication/raw/refactored/documents/images/figure1.png "VisiFicationInteractive Snapshot"


The forward and the backward sequences are sort of event points. 
Each event point belongs to one of the following types:
1. The robots
 * Stored as { < Index_Of_Robot, -1 > }
2. The vertices of the environment 
 * Stored as { < -2, Index_Of_Vertex > }
3. The vertices of the subdivision
 * In this case, the event points are stored as the pairs < -1, Index_Of_Edge > or < Index_Of_Robot, Index_Of_Reflex_Vertex > 
 which the second one is how a window related to a robot is defined in our system.


**VisiFicationHighPerformance** is an application which uses the same set of libraries as in VisiFicationInteractive but with predefined robots' motion algorithms.
This application move the robots based on their algorithms (e.g., Alpha algorithm used as a case study in the paper), and complete the state space incrementally (based on the given running time argument).

![VisiFicationHighPerformance Snapshot][VisiFicationHighPerformance]

[VisiFicationHighPerformance]: https://gitlab.com/mayssamin/VisiFication/raw/refactored/documents/images/figure2.png "VisiFicationHighPerformance Snapshot"

How to use
-----------

You can use the provided package in the 64-bit Debian-based distributions.

1. Install "VisiFicationProject-2.0.0-Linux.deb" package
 * sudo dpkg -i VisiFicationProject-2.0.0-Linux.deb 
2. To use the Interactive version of the application run:
		VisiFicationInteractive.2.0.0 [path-to-map-file]
	*A default map will be loaded if the path to map file is not provided as an argument
3. To use the application in high performance mode run:
		VisiFicationHP.2.0.0 path-to-map-file running-time-second



